"""
constants module for amplikyzer2
(c) 2011--2012 Sven Rahmann
"""

EXT_AMPLIKYZER = ".akzr"  # amplikyzer analysis file extension
EXT_AMPLIKYZER_GZIP = EXT_AMPLIKYZER + ".gz"
EXT_CONFIG = ".conf"  # config file extension (.cfg is problematic)

DEFAULT_ALIGNMENTPATH = "alignments"
DEFAULT_METHYLATIONPATH = "methylation"

DEFAULT_MAXFLOW = 4  # was 5
DEFAULT_CERTAINFLOW = 0.20     # 0.10, 0.15, (0.20) are reasonable
DEFAULT_MAYBEFRACTION = 0.10   # 0.1
DEFAULT_ALIGNMAYBEFLOW = 0.35  # was 0.25; 0.1 results in bad scores due to many insertions
DEFAULT_ALIGNTHRESHOLD = 0.65  # 0.65 is a reasonable value, fast and accurate
DEFAULT_ALIGNPSEUDOLENGTH = 10
DEFAULT_ALIGNMAXLENGTH = 350
DEFAULT_OPTIONALS_FILTERS = ((20, 3),)  # 3/20 = 15%

TAG_FWD = "FWD"
TAG_REV = "REV"
assert len(TAG_FWD) == len(TAG_REV)
TAG_LEN = len(TAG_FWD)
TAGSUFFIX_SEP = "___"
TAGSUFFIX_FWD = TAGSUFFIX_SEP + TAG_FWD
TAGSUFFIX_REV = TAGSUFFIX_SEP + TAG_REV
TAGSUFFIX_ANY = (TAGSUFFIX_FWD, TAGSUFFIX_REV)

FLOWCHARS_454 = "TACG"  # the order of nucleotide flows for 454jr
