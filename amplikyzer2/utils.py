"""
utilities module for amplikyzer2
(c) 2011--2012 Sven Rahmann
"""

import os       # for filename utilities
import os.path  # for filename utilities
import glob     # for filename utilities
from configparser import ConfigParser  # for reading config files
from time import time  # for TicToc
from itertools import chain, zip_longest, islice
import logging
from logging.handlers import QueueHandler, QueueListener
from multiprocessing import cpu_count, Pool, Manager
from contextlib import contextmanager
from operator import is_not
from functools import partial
from enum import Enum

from .exceptions import ArgumentError, MissingArgumentOut


class ConfigEnum(Enum):
    @classmethod
    def from_value(cls, value):
        try:
            return cls(value)
        except ValueError as e:
            values = "\n  ".join([""] + [str(e.value) for e in cls])
            raise ValueError("Need a value from:{}\nbut got '{}'".format(values, value)) from e


def uniques(iterable, key=None):
    """Return an iterator yielding unique elements from `iterable`.
    Like `iter(set(iterable))` but preserves order. All elements from `iterable` must be hashable.
    If `key is not None`, use `key(x)` for each `x in iterable` for comparisons (must be hashable).
    """
    unique_keys = set()
    if key is None:
        for x in iterable:
            if x not in unique_keys:
                unique_keys.add(x)
                yield x
    else:
        for x in iterable:
            k = key(x)
            if k not in unique_keys:
                unique_keys.add(k)
                yield x


def filter_is_not(iterable, obj=None):
    """Return an iterator yielding elements from `iterable which are not `obj`.
    Equivalent to `(x for x in iterable if x is not obj)`.
    """
    return filter(partial(is_not, obj), iterable)


def chop(iterable, chunksize):
    """Return iterator which chops `iterable` into lists of size `chunksize`."""
    if chunksize is not None:
        if not (isinstance(chunksize, int) and (chunksize > 0)):
            raise ValueError("chunksize must be a positive integer")
    it = iter(iterable)
    return iter(lambda: list(islice(it, chunksize)), [])


###################################################################################################
# time utilities

class TicToc():
    """Stopwatch which gives the elapsed time as a number (method `seconds`) or
    string (method `toc`) and can be reset (method `tic`)."""

    def __init__(self):
        """Initialize instance and set start time to current time."""
        self.precision = 2
        self.tic()

    def tic(self):
        """Reset to current time."""
        self.zero = time()

    def toc(self):
        """Return elapsed seconds since last reset as string (@ prepended)."""
        return "@{:.{}f}".format(self.seconds(), self.precision)

    def seconds(self):
        """Return elapsed seconds since last reset."""
        return time() - self.zero


###################################################################################################
# string utility functions

def positionlines(numbers, digits, gap_pos_char="."):
    """For a given iterable `numbers` of integers and a given number of `digits`,
    return a list of `digits` strings (lines) that,
    if printed in reverse order below each other,
    constitute the vertically written `numbers` (mod 10**digits).
    """
    lines = [[] for _ in range(digits)]
    if digits <= 0:
        return lines
    for i in numbers:
        if i < 0:
            for l in lines:
                l.append(gap_pos_char)
        elif i == 0:
            lines[0].append("0")
            for k in range(1, digits):
                lines[k].append(" ")
        else:
            for k in range(digits):
                c = str(i % 10) if i != 0 else " "
                lines[k].append(c)
                i = i // 10
    for k in range(digits):
        lines[k] = "".join(lines[k])
    return lines


def adjust_str_matrix(matrix, align=">", fill=" "):
    """Adjust columns of `matrix` according to `align` using `fill` for padding.

    `matrix`: two-dimensional array of strings, which is edited in place.
    """
    if align not in "><^":
        raise ValueError("align must be one of ><^")
    if len(fill) != 1:
        raise ValueError("fill must be a single character")
    column_widths = [max(map(len, column)) for column in zip_longest(fillvalue="", *matrix)]
    for row in matrix:
        row[:] = [
            '{:{}{}{}}'.format(cell, fill, align, width)
            for (cell, width) in zip(row, column_widths)]


def to_fasta(seq, linelen=60):
    i = 0
    pieces = []
    while True:
        piece = seq[i:i+linelen]
        if len(piece) == 0:
            break
        pieces.append(piece)
        i += linelen
    return "\n".join(pieces)


###################################################################################################
# filename utility functions

def filenames_from_glob(path, fnames, unique=False, allowzero=True):
    """Return list of filenames from glob (`path`/`fnames`), such as '*.txt'.

    `fnames` may be a list or a string.
    If `unique == True`, ensure that there exists at most one matching file.
    If `allowzero == False`, ensure that there exists at least one file.
    In violation, a `exceptions.ArgumentError` is raised.
    If both `unique == True` and `allowzero == False`,
      return the unique name as a string (not as 1-element list!).
    """
    if isinstance(fnames, str):
        fnames = [fnames]
    pattern = [os.path.join(path, f) if f != "-" else "-" for f in fnames]
    files = (glob.glob(p) if p != "-" else "-" for p in pattern)
    files = list(chain.from_iterable(files))

    if unique and not allowzero:
        if len(files) != 1:
            raise ArgumentError("no files or more than one file found: {}".format(pattern))
        return files[0]
    elif unique:
        if len(files) > 1:
            raise ArgumentError("more than one file found: {}".format(pattern))
    elif not allowzero and not files:
        raise ArgumentError("no files found: {}".format(pattern))
    return files


def get_outname(argout, path, filenames, extension, option="--out"):
    if argout == "-":
        return "-"
    if argout is not None:
        # not requesting stdout, so prepend path
        return os.path.join(path, argout)
    if not filenames:
        return "-"
    if len(filenames) == 1:
        base, ext = os.path.splitext(filenames[0])
        if ext == ".gz":
            base, ext = os.path.splitext(base)
        return base + extension
    raise MissingArgumentOut("must specify option '{}' for >= 2 input files".format(option))


def ensure_directory(directory):
    """Ensure that directory `directory` exists.

    It is an error to pass anything else than a directory string.
    """
    if not directory:
        return
    directory = os.path.abspath(directory)
    os.makedirs(directory, exist_ok=True)


###################################################################################################
# config file reading

def read_config_files(path, conf):
    """Read all config files given by `args.path`, `args.conf` and return the
    `configparser.ConfigParser` object.
    """
    configfiles = filenames_from_glob(path, conf)
    parser = ConfigParser(empty_lines_in_values=False, interpolation=None)
    parser.optionxform = str  # allow case-sensitive keys
    parser.read(configfiles, encoding="utf-8")
    return parser


def labels_from_config(configinfo):
    """Parse labels from `configinfo` object.
    MID, LOCUS = Patient Name
    """
    labels = dict()
    if "LABELS" not in configinfo:
        return labels  # empty dictionary if no labels present
    for key, value in configinfo.items("LABELS"):
        # key must be "MID, LOCUS" or "MID" alone
        # the presence of the comma distinguishes both cases
        if "," in key:  # "MID, LOCUS"
            mid, locus = key.split(",")
            key = (mid.strip(), locus.strip())
        labels[key] = value
    return labels


def get_label(labels, mid, locus=None):
    """Return label from `dict` `labels` for `(mid, locus)`.

    If there is no label for `(mid, locus)` return label for `mid`.
    If there is no label for `mid` either simply return `mid` itself."""
    key = (mid, locus)  # never exists when locus is None
    if key in labels:
        return labels[key]
    return labels.get(mid, mid)


###################################################################################################
# multiprocessing utilities

def verbose_pool_map(map_func, func, iterable, *args, **kwargs):
    """Apply `func` to `iterable` through `map_func`; re-raise exceptions which
    are raised by `iterable`.

    `multiprocessing.Pool.imap` may not pass through an exception from
    generator `iterable` to caller, so this function saves and re-raises it.
    Example usage:
        def generator(n, x):
            for i in range(n):
                if i == x:
                    raise RuntimeError
                yield i
        def func(i):
            return 2 * i
        with multiprocessing.Pool(2) as pool:
            n, x, c = 10, 2, 4
            assert x < c <= n
            sum(pool.imap(func, generator(n, x), chunksize=c))
            # no RuntimeError raised
            sum(verbose_pool_map(pool.imap, func, generator(n, x), chunksize=c))
            # RuntimeError raised through verbose_pool_map
    """
    caught_exception = []

    def exception_catcher():
        try:
            yield from iterable
        except Exception as e:
            caught_exception[:] = [e]
            raise
    yield from map_func(func, exception_catcher(), *args, **kwargs)
    if caught_exception:
        raise caught_exception[0]


class LogRecordDelegator:
    """`logging.Handler`-like class for `logging.handlers.QueueListener`.

    Forwards all `logging.LogRecord` instances to their appropriate loggers.
    """
    def handle(self, record):
        logging.getLogger(record.name).handle(record)


def init_queue_logging(log_queue, level=logging.INFO):
    """Remove all logging handlers and register a `logging.handlers.QueueHandler`
    for `log_queue` on the root logger.

    Must not be called in main process but only in subprocesses during their
    initialization.
    """
    root_logger = logging.getLogger()
    loggers = chain([root_logger], logging.Logger.manager.loggerDict.values())
    for logger in loggers:
        if isinstance(logger, logging.PlaceHolder):
            continue
        for handler in logger.handlers.copy():
            logger.removeHandler(handler)
    root_logger.addHandler(QueueHandler(log_queue))
    root_logger.setLevel(level)


@contextmanager
def get_pool(pool_size):
    """Return a tuple of actual pool size and either `multiprocessing.Pool` or `PoolDummy`.

    Only return `PoolDummy` if `pool_size` is `None` or 1.
    If `pool_size` is 0 `multiprocessing.cpu_count()` is used instead.
    Closes pool on `__exit__`.
    """
    if pool_size == 0:
        pool_size = cpu_count()
    if pool_size is None:
        pool_size = 1
    if pool_size == 1:
        yield pool_size, PoolDummy()
    else:
        with Manager() as manager:
            log_queue = manager.Queue()
            log_listener = QueueListener(log_queue, LogRecordDelegator())
            log_listener.start()
            with Pool(pool_size, init_queue_logging, (log_queue,)) as pool:
                yield pool_size, pool
            log_listener.stop()


class AsyncResultDummy:
    def __init__(self, data, success):
        self._data = data
        self._success = success

    def get(self, timeout=None):
        if not self._success:
            raise self._data
        return self._data

    def wait(self, timeout=None):
        return

    def ready(self):
        return True

    def successful(self):
        return self._success


class PoolDummy:
    def __init__(self, *args, **kwargs):
        pass

    def __enter__(self):
        return self

    def __exit__(self, *args):
        return False

    def imap(self, func, iterable, chunksize=None):
        return map(func, iterable)

    def apply_async(self, func, args=None, kwargs=None, callback=None, error_callback=None):
        if args is None:
            args = ()
        if kwargs is None:
            kwargs = dict()
        try:
            result = func(*args, **kwargs)
        except Exception as e:
            if error_callback is not None:
                error_callback(e)
            return AsyncResultDummy(e, False)
        else:
            if callback is not None:
                callback(result)
            return AsyncResultDummy(result, True)
