from .main import main, get_argument_parser  # NOQA: F401

from ._version import get_versions
__version__ = get_versions()["version"]
del get_versions
