"""
exceptions module for amplikyzer2
"""


class ArgumentError(RuntimeError):
    """Illegal combination of arguments given."""


class MissingArgumentOut(ArgumentError):
    """Must specify filename when more than one file of given group is present."""


class FormatError(RuntimeError):
    """
    An error in the format of a configuration or analysis file,
    or an unrecognized tag has appeared somewhere.
    """


class ConfigFormatError(FormatError):
    """
    An error in the format of a configuration file.
    """


class AkzrFormatError(FormatError):
    """
    An error in the format of an analysis file.
    """


class AkzrOutputError(FormatError):
    """
    An error while writing an analysis file.
    """
