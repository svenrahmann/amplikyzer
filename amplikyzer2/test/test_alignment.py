# from amplikyzer import scoring
#
# def test_alignment():
#     ref  = "CGAGCAGGCGGCATCTCTAGCTTGCCGCGGCCGCGATGTCCCC-CGCCTGTCTGCGAA-TGCGG-CGTAGCGGGTAGACATGGAGGGCTTTCGGCATCGTCAGAGTGGCCAGTGTGCGCGTCCTT-GCCCATCAGGCGGGGGGG-CT--GTGGGGG-AGG-AGAGG-AGGCAGTGGAGGG"
#     read = "CGAGTAGGCGGTATTTTTAGTTTGTCGCGGTCGCGATGTTTTT+aGTTTGTTTGCGAAcTGCGGtCGTAGCGGGTAGATATGGAGGGTTTTCGGTATCGTTAGAGTGGTTAGTGTGCGCGTTTTTaGTTTATTAGGCGGGGG--+TTtaGTGGGGGtAGGtAGAGGtAGGTAGTGGAGGG"
#     #ref  = "CGAGCAGGCGGCATCTCTAGCTTGCCGCGGCCGCGATGTCCCCC--GCCTGTCTGCGAA-TGCGG-CGTAGCGGGTAGACATGGAGGGCTTTCGGCATCGTCAGAGTGGCCAGTGTGCGCGTCCTT-GCCCATCAGGCGGGGGGG-CT--GTGGGGG-AGG-AGAGG-AGGCAGTGGAGGG"
#     #read = "CGAGTAGGCGGTATTTTTAGTTTGTCGCGGTCGCGATGTTTTT-+aGTTTGTTTGCGAAcTGCGGtCGTAGCGGGTAGATATGGAGGGTTTTCGGTATCGTTAGAGTGGTTAGTGTGCGCGTTTTTaGTTTATTAGGCGGGGG--+TTtaGTGGGGGtAGGtAGAGGtAGGTAGTGGAGGG"
#     xref = ref.replace("-", "")
#     xread = read.replace("-", "")
#     scores = scoring.flowdna_bisulfiteCT
#     spossible = len(ref) * scores.maxscore
#     sthreshold = int(1 + 0.3 * spossible)
#     (s,index,al) = scoring.align_genomic_to_flowdna(xref, xread, scores, sthreshold)
#     aref = al[1]
#     aread = al[0]
#     print(aref[:80])
#     print(aread[:80])
#     print(s)
#     total = showscores(ref, read, scores)
#     print(total)
#
#
# def showscores(ref, read, scores):
#     sc = scores.score
#     insflow = scores.insflow
#     delflow = scores.delflow
#     total = 0
#     for (i,(g,f)) in enumerate(zip(ref,read)):
#         if f == "-":
#             j = i - 1
#             while read[j] == "-":  j -= 1
#             fl = read[j]
#             j = i + 1
#             while read[j] == "-":  j += 1
#             fr = read[j]
#             s = insflow[fl][fr][g]
#             print(g,f, s, "fl,fr,g =", fl,fr,g)
#         elif g == "-":
#             j = i - 1
#             while ref[j] == "-":  j -= 1
#             gl = ref[j]
#             j = i + 1
#             while ref[j] == "-":  j += 1
#             gr = ref[j]
#             s = delflow[f][gl][gr]
#             print(g,f, s, "f,gl,gr =", f,gl,gr)
#         else:
#             s = sc[f][g]
#             print(g,f, s)
#         total += s
#     print()
#     return total
#
#
# if __name__ == "__main__":
#     test_alignment()
