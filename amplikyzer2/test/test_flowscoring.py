# from amplikyzer.scoring import align_genomic_to_flows, ScorematrixFlowIUPAC
#
# def test_flowscoring_interactive(genomic, flows):
#     print("Genomic:", genomic)
#     print("Flows:", flows)
#     score, j, ali = align_genomic_to_flows(genomic, flows, suppress_gaps=True)
#     print("Score: {}".format(score))
#     print("Best macht ends at flow {} (of {})".format(j, len(flows)))
#     print("Alignment:")
#     print("G:", ali[1])
#     print("F:", ali[0])
#
# def test_scorefunction_interactive(scorematrix, g, fc, f):
#     s = scorematrix.compute_score(fc, f, g)
#     print("Scoring {} against {}{:.2f}: {:.1f}".format(g,fc,f,s))
#
#
# if __name__ == "__main__":
#     # assuming TACG... flowchars
#     test_flowscoring_interactive("TAAGGATGTCC", [100,210,110,220,310,10,270,20])
#
#     tests = [ ("T", "T", 1.00),
#               ("AA", "A", 2.10),
#               ("", "C", 1.10),
#               ("GG", "G", 2.20),
#               #("A", "A", 0.00),
#               #("G", "C", 1.10),
#               #("GA", "G", 2.20),
#               ("TGT", "T", 3.10),
#               ("", "A", 0.10),
#               ("CC", "C", 3),
#               ("", "G", 0.20),
#             ]
#     sm = ScorematrixFlowIUPAC()
#     for g, fc, f in tests:
#         test_scorefunction_interactive(sm, g, fc, f)
