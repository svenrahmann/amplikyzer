import os.path
from functools import partial
# from itertools import islice
from collections import namedtuple, Counter
import logging
from abc import ABC, abstractmethod

import numpy as np
from numba import njit, generated_jit
from numba.types import NoneType

from ..exceptions import ConfigFormatError, MissingArgumentOut
from ..constants import (
    EXT_AMPLIKYZER, EXT_AMPLIKYZER_GZIP, TAG_FWD, TAG_REV, TAG_LEN, DEFAULT_ALIGNTHRESHOLD,
)
from .. import utils    # general utilities
from ..alphabets import encoding, encode, revcomp, SENTINEL, GENOMIC, VOID, GAP

from .common import (
    State, Rc, Bis, Special, Match,
    Directions, opposite_direction, tag_to_direction,
    DEFAULT_ANALYSIS_OUTPUT_NAME,
)
from .output import AkzrWriter
from .scoring.flowdna import (
    cut_indexed_fdna_prefix, genomic_to_index_map, match_reference_to_flowdna)
from .scoring.dna import genomic_to_dna5_index_map, match_reference_to_dna


def buildparser(p):
    p.add_argument(
        "--parallel", "-p", type=int, nargs="?", const=0, metavar="INT",
        help="number of processors to use for analysis [0=max]")
    p.add_argument(
        "--force", "-f", action="store_true", default=False,
        help="force overwriting existing {} file".format(EXT_AMPLIKYZER))
    p.add_argument(
        "--alignthreshold", type=float, default=DEFAULT_ALIGNTHRESHOLD, metavar="FLOAT",
        help=("threshold fraction of maximum possbile score for alignments"
              " [{}]".format(DEFAULT_ALIGNTHRESHOLD)))
    p.add_argument(
        "--out", "-o", metavar="FILE",
        help="output file; '-' for stdout, i.e. console / log window")
    p.add_argument(
        "--debug", "-D", metavar="ID",
        help="specify a single read ID for debugging")
    p.add_argument(
        "--omit", nargs="*",
        choices=tuple(map(str, (State.FILTERED, State.UNMAPPED,
                                State.UNALIGNED, State.INSUFFICIENT_COVERAGE))),
        default=[],
        help=("omit reads in output that are filtered, unmapped, unaligned"
              " or have less coverage than demanded by --badcoverage"
              " (note: 'statistics' subcommand won't consider those omissions)"))
    p.add_argument(
        "--badcoverage", type=float, metavar="INT/FLOAT", default=-1,
        help=("max number (>=1) or rate (<1.0) of unsequenced ROI bases"
              " (unlimited if less than 0) [-1]"))


FileNames = namedtuple("FileNames", ["reads", "config", "out"])


class Analyzer(ABC):
    @property
    @abstractmethod
    def READ_FILE_TYPE(self):
        pass  # in {"SFF", "FASTQ"}

    @property
    @abstractmethod
    def READ_FILE_EXT(self):
        pass  # in {".sff", ".fastq"}

    def __init__(self, args):
        self.args = args
        self.matrices = self.get_score_matrices()

    def create_writer(self, fnout, force, omit_states):
        return AkzrWriter(fnout, force, omit_states)

    def run(self):
        """analyze read files, given command line arguments in args"""
        args = self.args
        # check output file
        try:
            self.filenames = self.get_filenames()
        except MissingArgumentOut:
            logging.getLogger(__name__).warn(
                "Must specify '--out outfile' for >= 2 {} files".format(self.READ_FILE_EXT))
            return
        if not self.filenames.reads:
            logging.getLogger(__name__).warn(
                "No {} files found; nothing to do; check --path ={}".format(
                    self.READ_FILE_EXT, args.path))
            return

        self.config = self.read_config(self.filenames.config)

        fnout = self.filenames.out
        try:
            writer = self.create_writer(fnout, args.force, args.omit)
        except FileExistsError:
            logging.getLogger(__name__).warn(
                "Output file '{}' exists, nothing to do.\n"
                "Use --force to re-analyze.".format(fnout))
            return
        else:
            with writer:
                self.analyze_read_files(writer)

    @abstractmethod
    def read_config(self, config_filenames):
        """Read all config files into the config data structure and return it."""

    def get_common_input_name(self, read_file_names):
        if len(read_file_names) > 1:
            # no common input name found, so return default output name
            return [DEFAULT_ANALYSIS_OUTPUT_NAME]
        return read_file_names

    def get_filenames(self):
        """Return a namedtuple with attributes
          `reads`: `list` of `str`,
          `config`: `list` of `str`,
          `out`: `str`,
        all of which specify filenames derived from `self.args`.
        """
        args = self.args
        path = args.path

        reads = sorted(utils.filenames_from_glob(path, args.reads))
        common_read_name = [os.path.join(path, n) for n in self.get_common_input_name(reads)]
        out = utils.get_outname(args.out, path, common_read_name, EXT_AMPLIKYZER_GZIP, "--out")
        config = utils.filenames_from_glob(path, args.conf)
        return FileNames(reads=reads, config=config, out=out)

    def get_element_info(self, string):
        return ElementInfo.from_string(string)

    def get_elements(self, pseudolength=0, isflowdna=False):
        """Process configuration information and return a `tuple` of `Element` instances.

        `self.config`: `configparser.ConfigParser`
        `self.matrices`: `.scoring.common.ScoringMatrices`
        `pseudolength`: `int`, pseudo length for flowdna mapping / alignment
        `isflowdna`: `bool`, flag to indicate flowdna mapping / alignment
        """
        config = self.config
        matrices = self.matrices
        # obtain elementinfo, information on the elements of each read
        _, elementinfo_strings = zip(*sorted((int(j), e) for (j, e) in config.items("ELEMENTS")))
        elementinfo = [self.get_element_info(string) for string in elementinfo_strings]
        if any(info.section == "ELEMENTS" for info in elementinfo):
            raise ConfigFormatError("config section [ELEMENTS] cannot be part of a read")
        # get target regions
        targets = [info.special == Special.TARGET for info in elementinfo]
        if sum(targets) != 1:
            raise ConfigFormatError("must have exactly one special target element!")
        # process each config file section, according to elementinfo

        assert isflowdna or pseudolength == 0, "pseudolength only used for flowdna"
        elements = []
        for info in elementinfo:
            element = self.make_element(
                list(config.items(info.section) if config.has_section(info.section) else []),
                info, matrices, pseudolength, isflowdna)
            elements.append(element)
        target_element = elements[targets.index(True)]
        return tuple(elements), target_element

    def make_element(self, entries, info, matrices, pseudolength, isflowdna):
        classes = {
            Special.NONE: Element,
            Special.TAG: Element,
            Special.TARGET: TargetElement,
        }
        cls = classes[info.special]
        return cls(entries, info, matrices, pseudolength, isflowdna)

    def analyze_read_files(self, writer):
        """Analyse several read files given by `self.filenames.reads`.
        """
        self.clock = utils.TicToc()
        filenames = self.filenames
        logger = logging.getLogger(__name__)
        logger.info("\n  ".join([
            "reading files:",
            "{}S: {}".format(self.READ_FILE_TYPE, ", ".join(filenames.reads)),
            "configs: {}".format(", ".join(filenames.config)),
            "writing to: {}".format(filenames.out)]))
        # parse config files
        self.elements, self.target_element = self.get_elements()

        writer.write_header(self.filenames, self.READ_FILE_TYPE, self.args, self.elements)
        num_reads, read_files = self.get_read_files()
        self.num_reads = num_reads
        self.num_reads_processed = 0
        self.state_counter = Counter()
        num_read_files = 0
        for read_file in read_files:
            num_read_files += 1
            self.analyze_read_file(read_file, writer)
        if num_read_files > 1:
            assert sum(self.state_counter.values()) == self.num_reads_processed
            self.log_state_counter(self.state_counter)

    def log_state_counter(self, state_counter):
        logger = logging.getLogger(__name__)
        total = sum(state_counter.values())
        for state, count in sorted(state_counter.items(), key=lambda x: x[0].value):
            logger.info("{state}: {count} / {total} ({fraction:.2%})".format(
                state=state, count=count, total=total, fraction=(count / total)))

    @abstractmethod
    def get_score_matrices(self):
        pass

    @abstractmethod
    def get_read_files(self):
        """Return `num_reads, read_files`."""

    @abstractmethod
    def analyze_read_file(self, read_file, writer):
        """Calls `self.process_reads(reads, elements, target_element, writer)`."""

    @abstractmethod
    def create_worker(self, config, reportinterval=None):
        """Return an instance of `AnalyzeWorker` initialized with `config`."""

    def process_reads(self, reads, elements, target_element, writer):
        args = self.args
        clock = self.clock
        result_formatter = writer.create_result_formatter(self.elements, self.target_element)
        config = WorkerConfig(
            self.num_reads_processed, self.num_reads, args,
            elements, target_element, self.matrices, clock, result_formatter)
        logger = logging.getLogger(__name__)
        # reads = islice(reads, 100000)
        worker = self.create_worker(config)
        enum_reads = enumerate(reads)
        # determine size of process pool for parallel or sequential analysis
        # generate and report results
        with utils.get_pool(args.parallel) as (pool_size, pool):
            if pool_size < 2:
                logger.info("analyzing reads sequentially...")
            else:
                logger.info("analyzing reads using {} processes...".format(pool_size))
            state_counter = Counter()
            results = utils.verbose_pool_map(pool.imap, worker, enum_reads, chunksize=1000)
            for state, result in results:
                state_counter[state] += 1
                self.state_counter[state] += 1
                writer.write_entry(state, result)
                self.num_reads_processed += 1
        self.log_state_counter(state_counter)
        logger.info("done")


###################################################################################################
# analyse all files

class Read(ABC):
    def __init__(self, t, source, args):
        self.t = t
        self.source = source
        self.encoded = self.encode(args)
        self.ix_encoded = self.ix_encode()

    @abstractmethod
    def encode(self, args):
        """Return encoded `self.source`."""

    @abstractmethod
    def ix_encode(self):
        """Return encoded `self.source` with each base being an index for the score matrix."""


WorkerConfig = namedtuple('WorkerConfig', [
    'num_reads_processed',
    'num_reads',
    'args',
    'elements',
    'target_element',
    'matrices',
    'clock',
    'result_formatter',
])


class AnalyzeWorker(ABC):
    def __init__(self, config, reportinterval=None):
        """
        config = (num_reads, args, elementinfo, targets, clock):
          - num_reads: total number of reads, such that 0 <= i < num_reads
          - args: argparsed command line arguments
          - elementinfo = (info_0, info_1, ...);
              info_i = (name_i, section_i, rc_i, bis_i, ETC.);
          - targets: dict of target (ROI) sequences, indexed by name
          - matrices: score matrices
          - clock: the running clock
        """
        self.reportinterval = reportinterval if reportinterval else 10000
        self.args = config.args
        self.elements = config.elements
        self.target_element = config.target_element
        self.num_reads_processed = config.num_reads_processed
        self.num_reads = config.num_reads
        self.matrices = config.matrices
        self.clock = config.clock
        self.result_formatter = config.result_formatter
        self.debug = config.args.debug

    @abstractmethod
    def create_read(self, t, source, args):
        """Return an instance of `Read`."""

    def __call__(self, t_read):
        """score and align a FlowDNA read against the expected genomic elements.
          - t: running number of the read in the sff file
          - read: the read
        """
        t, read = t_read
        if (not self.debug) and (t > 0 and t % self.reportinterval == 0):
            total_t = t + self.num_reads_processed
            remaining = self.clock.seconds() * max(0, self.num_reads / total_t - 1)
            logging.getLogger(__name__).info(
                "#{} / {} -> {:.0f} seconds remaining".format(total_t, self.num_reads, remaining))

        if (not self.debug) or (read.name == self.debug):
            state, output = self.process_read(t, read)
        else:  # debug mode, but this is not the read: skip
            state, output = [], State.SKIPPED

        return state, "\n".join(output)

    def process_read(self, t, read):
        """Process the `t`-th read `read`:
        Return: tuple (result, state), where
          - result: list of strings to output to analysis file
          - state: State {filtered, unmapped, unaligned, aligned}
        """
        read = self.create_read(t, read, self.args)
        (state, result) = self.map_and_align_read(read)
        output = self.result_formatter.format(read, state, result)
        return state, output

    def map_and_align_read(self, read):
        if len(read.encoded) == 0:
            return State.FILTERED, (None, [], None)
        (mappings, targets) = self.map_read(read)
        assert len(self.elements) == len(mappings)
        if not targets:
            return State.UNMAPPED, (mappings, targets, None)
        state, align_result = self.align_read(read, targets)
        return state, (mappings, targets, align_result)

    def map_read(self, read):
        assert len(read.encoded) > 0
        # Try to map each element
        mappings = []
        direction = Directions.ALL
        targets = []
        for element in self.elements:
            element_mappings = self.map_element(read, direction, element)
            mappings.append(element_mappings)
            # Treat special elements
            if element.info.special == Special.TAG:
                if element_mappings and any(element_mappings):
                    # extract tag prefix
                    read_matches = next(filter(None, element_mappings))
                    tag_target = read_matches[0].target
                    tag_i = tag_target[1]
                    tag = element.names[tag_i][:TAG_LEN]
                    direction = tag_to_direction.get(tag, Directions.ALL)
            elif element.info.special == Special.TARGET:
                targets = self.get_targets(element, element_mappings)
        # done processing each element
        # check for tag/target match or mismatch
        if direction != Directions.ALL:
            # remove direction-mismatching targets from targets
            targets = list(filter(lambda target: target[0] == direction, targets))
        return mappings, targets

    def map_element(self, read, direction, element):
        return [element.match(read.ix_encoded, direction)]

    def get_targets(self, element, matches):
        if not (matches and any(matches)):
            return ()
        scored_targets = []
        for read_i, r_matches in enumerate(matches):
            if not r_matches:
                continue
            for match in r_matches:
                (direction, i) = match.target
                if read_i > 0:
                    direction = opposite_direction[direction]
                    if element.info.special == Special.TAG:
                        i = len(element.names) - i
                scored_targets.append((-match.score, direction, i))  # prepend `-score` for sorting
        targets = ((direction, i) for (_, direction, i) in sorted(scored_targets))
        targets = list(utils.uniques(targets))
        return targets

    def align_read(self, read, targets):
        # align read full read against selection of targets
        # target_element.get(*target) is a ROI sequence
        # TODO: change that to be a triple (fullseq, primerlen, roilen),
        # then extract the ROI (but take care of + and gaps)
        target_element = self.target_element

        if not target_element:
            return State.UNALIGNED, None
        best_score = (0, 0)
        best_target = None
        best_alignment = None
        for target in targets:
            (direction, index) = target
            roi = target_element.get_genomic(direction, index)
            (score, alignment) = self.align(roi, read, direction=direction)
            if score[0] > best_score[0]:
                best_score = score
                best_target = target
                best_alignment = alignment
        if best_alignment is None:
            return State.UNALIGNED, None
        max_voids = self.args.badcoverage
        if max_voids >= 0:
            (aligned_reference, aligned_read) = best_alignment
            if max_voids < 1:
                max_voids *= ((aligned_reference != GAP) & (aligned_reference != VOID)).sum()
            num_voids = (aligned_read == VOID).sum()
            if num_voids > max_voids:
                return State.INSUFFICIENT_COVERAGE, (best_target, best_score, best_alignment)
        return State.ALIGNED, (best_target, best_score, best_alignment)

    @abstractmethod
    def align(self, genomic, read, direction=None):
        pass


###################################################################################################
# elements of a read
# can be customized in a config file with an [ELEMENTS] section
# formatted as described here

class ElementInfo(namedtuple("ElementInfo", [
        "name", "section", "rc", "bis",
        "startpos", "tolerance", "threshold",
        "required", "special"])):
    __slots__ = ()
    _field_convs = (
        str, str, Rc.from_value, Bis.from_value,
        int, int, float,
        int, Special.from_value)

    @classmethod
    def from_string(cls, string):
        fields = list(map(str.strip, string.split(",")))
        try:
            return cls.from_strings(fields)
        except ValueError as e:
            raise ConfigFormatError("{}.\nIn string '{}'".format(e, string)) from e

    @classmethod
    def from_strings(cls, fields):
        return cls._make(conv(field) for conv, field in zip(cls._field_convs, fields))


class Element:
    """
    """
    _dnatrans = str.maketrans("U", "T", ",; \n\t")
    _allowed_genomic = frozenset(GENOMIC)

    def __init__(self, entries, info, matrices, pseudolength, isflowdna):
        self.isflowdna = isflowdna
        self.info = info
        self.pseudolength = pseudolength
        try:
            names, seqs = self._parse_entries(entries)
            self.names = names
        except ConfigFormatError as e:
            raise ConfigFormatError("[{}] section: {}".format(info.section, e)) from e
        self._set_genomics(seqs)
        self._set_scorematrices(matrices)

    def __len__(self):
        return len(self.names)

    def _parse_entries(self, entries):
        names, seqs = zip(*entries) if entries else ([], [])
        if self.info.special == Special.TAG:
            if any(not name.startswith((TAG_FWD, TAG_REV)) for name in names):
                raise ConfigFormatError(
                    "All tags must start with '{}' or '{}'.".format(TAG_FWD, TAG_REV))
        return names, seqs

    def _convert_genomic(self, seq):
        # replace U (RNA) by T (DNA) and remove spaces, commas etc.
        seq = seq.strip().upper().translate(self._dnatrans)
        # check for correctness
        for c in seq:
            if c not in self._allowed_genomic:
                raise ConfigFormatError("non-DNA character '{}' found".format(c))
        return seq

    def _set_genomics(self, seqs):
        seqs = [encode(self._convert_genomic(seq)) for seq in seqs]
        lengths = np.fromiter(map(len, seqs), dtype=np.int32)
        num_seqs = lengths.shape[0]
        max_len = lengths.max() if num_seqs > 0 else 0
        genomics = np.full((1, num_seqs, max_len), SENTINEL, dtype=encoding.dtype)
        for i in range(num_seqs):
            genomics[0, i, :lengths[i]] = seqs[i]
        if self.info.rc == Rc.OK:
            genomics = np.vstack([genomics, genomics])
            for i in range(num_seqs):
                genomics[1, i, :lengths[i]] = revcomp(seqs[i])
        if self.isflowdna:
            indexed_genomics = genomic_to_index_map[genomics]
        else:
            indexed_genomics = genomic_to_dna5_index_map[genomics]
        self.lengths = lengths
        self.genomics = genomics
        self.indexed_genomics = indexed_genomics

    def _set_scorematrices(self, matrices):
        if self.info.bis == Bis.NONE:
            scorematrices = (matrices.standard, matrices.standard)
        else:
            scorematrices = (matrices.bisulfiteCT, matrices.bisulfiteGA)
        if self.info.rc != Rc.OK:
            scorematrices = (scorematrices[0], )
        array = partial(np.array, dtype=scorematrices[0].matrix.dtype)
        self.matchflows = array([s.matrix for s in scorematrices])
        self.insflows = array([s.insflow_array for s in scorematrices])
        self.delflows = array([s.delflow_array for s in scorematrices])
        self.maxscores = array([s.maxscore for s in scorematrices])

    def get_genomic(self, direction, index):
        return self.genomics[direction, index, :self.lengths[index]]

    def match(self, ix_read, direction):
        return match_element(
            ix_read, direction, self.indexed_genomics, self.lengths,
            self.info.startpos, self.info.tolerance,
            self.info.threshold, self.pseudolength,
            self.matchflows, self.insflows, self.delflows, self.maxscores,
            self.isflowdna)


class TargetElement(Element):
    # def __init__(self, entries, info, matrices, pseudolength, isflowdna):
    #     super.__init__(self, entries, info, matrices, pseudolength, isflowdna)
    #     self.roi_offsets = []
    #     self.roi_lengths = []

    def _parse_entries(self, entries):
        primers = []
        rois = []
        for name, seq in entries:
            try:
                forward_primer, roi, reverse_primer = self.parse_locus(seq)
            except ConfigFormatError as e:
                raise ConfigFormatError("{}: {}".format(name, e)) from e
            primers.append((forward_primer, reverse_primer))
            rois.append((name, roi))
        self.primers = primers
        entries = rois
        names, seqs = zip(*entries) if entries else ([], [])
        return names, seqs

    def parse_locus(self, seq):
        # split into forward primer, ROI, reverse primer
        fsr = seq.split(",")
        if len(fsr) != 3:
            raise ConfigFormatError("(not 3 parts, but {}) {}".format(len(fsr), fsr))
        (forward_primer, roi, reverse_primer) = fsr
        forward_primer = self._convert_genomic(forward_primer)
        reverse_primer = self._convert_genomic(reverse_primer)

        return (forward_primer, roi, reverse_primer)


@njit(cache=True)
def match_element(
        ix_read, direction, indexed_genomics, lengths,
        begin, tol, threshold, pseudolength,
        matchflows, insflows, delflows, maxscores, isflowdna):
    if isflowdna is not None:
        ix_read = cut_indexed_fdna_prefix(ix_read, begin)
    else:
        ix_read = ix_read[begin:]
    if tol == -1:
        tol = len(ix_read)
    else:
        tol = max(0, tol - begin)
    num_directions = matchflows.shape[0]
    if direction == -1:
        directions = range(num_directions)
    else:
        if direction >= num_directions:
            raise RuntimeError("invalid tag")
        directions = range(direction, direction+1)
    matches = []
    for direction in directions:
        direction_matches = match_genomics_to_read(
            ix_read, indexed_genomics[direction], lengths,
            tol, threshold, pseudolength,
            matchflows[direction], insflows[direction], delflows[direction], maxscores[direction],
            isflowdna)
        for (score, score_possible, column, row, i) in direction_matches:
            matches.append(Match(score, score_possible, column, row, (direction, i)))
    matches.sort(reverse=True)
    # return at most 2 matches
    if not matches:
        return matches
    best_score = matches[0].score
    if best_score <= 0:
        return matches[:1]
    return matches[:2]


@generated_jit(nopython=True, cache=True)
def match_genomics_to_read(
        ix_read, indexed_genomics, lengths,
        tol, threshold, pseudolength,
        matchflow, insflow, delflow, maxscore, isflowdna):
    if isinstance(isflowdna, NoneType):
        match_reference_to_read = match_reference_to_dna
        endgapfree = True
    else:
        match_reference_to_read = match_reference_to_flowdna
        endgapfree = False

    def match_genomics_to_read(
            ix_read, indexed_genomics, lengths,
            tol, threshold, pseudolength,
            matchflow, insflow, delflow, maxscore, isflowdna):
        nseqs = lengths.shape[0]
        matches = []
        if len(ix_read) == 0:
            return matches
        for i in range(nseqs):
            m = lengths[i]
            indexed_genomic = indexed_genomics[i, :m]
            indexed_read = ix_read[:(m + tol)]
            (score, score_possible, column, row) = match_reference_to_read(
                indexed_genomic, indexed_read, threshold,
                matchflow, insflow, delflow, maxscore,
                pseudolength, endgapfree)
            if score > 0:
                matches.append((score, max(1, score_possible), column, row, i))
        return matches
    return match_genomics_to_read
