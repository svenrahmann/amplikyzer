# amplikyzer2.analysis module
# implements the 'analyze-sff' subcommand
# (c) 2011--2012 Sven Rahmann

"""
Analyze .sff files.
Split each flowgram sequence in the .sff files into
key, mid, tag, primer, region of interest
and align with given genomic reference sequence.
Write an .akzr file containing the analysis results and alignments.

Use the 'statistics' subcommand to obtain a summary of the results.
Use the 'align' subcommand to format the alignments for output.
Use the 'methylation' subcommand to do methylation analysis on the alignments.
"""

import logging

from ..constants import (
    DEFAULT_MAXFLOW, DEFAULT_CERTAINFLOW, DEFAULT_MAYBEFRACTION,
    DEFAULT_ALIGNMAYBEFLOW, DEFAULT_ALIGNPSEUDOLENGTH, DEFAULT_ALIGNMAXLENGTH,
)
from .. import utils    # general utilities
from .. import flowdna
from ..sff import SffFile
from .common import Directions, Bis
from .scoring.flowdna import (
    matrices as flowdna_matrices, align_genomic_to_flowdna, flowdna_to_index_map)
from .scoring.flow import matrices as flow_matrices, align_genomic_to_flows
from . import base


CONFIG_STANDARD_ELEMENTS_SFF = """
[ELEMENTS]
# syntax is:
# order = ( name,  config.SECTIONHEADER,  rc_{no|ok},  bis_{none|tagged|all},
#           startpos,  tolstartpos|-1,  threshold,
#           required=0|1,  special_{none|tag|target} )
1 = key,   KEYS, rc_no, bis_none,    0,  1,  0.7,   0, special_none
2 = mid,   MIDS, rc_no, bis_none,    4,  7,  0.7,   0, special_none
3 = tag,   TAGS, rc_no, bis_none,   12, 34,  0.7,   0, special_tag
4 = locus, LOCI, rc_ok, bis_tagged, 28, -1,  0.7,   1, special_target
"""


def buildparser(p):
    base.buildparser(p)
    p.add_argument(
        "--reads", "--sff", "-s", nargs="+", default=["*.sff"], metavar="FILE",
        help="SFF file(s) to analyze")
    p.add_argument(
        "--alignflows", action="store_true", default=False,
        help="use experimental flow alignment instead of flowdna")
    p.add_argument(
        "--maxflow", "-M", type=int, default=DEFAULT_MAXFLOW, metavar="INT",
        help="maximum flow intensity before cutoff [{}]".format(DEFAULT_MAXFLOW))
    p.add_argument(
        "--certainflow", "-c", type=float, default=DEFAULT_CERTAINFLOW, metavar="FLOAT",
        help="fractional flow intensity considered certain [{}]".format(DEFAULT_CERTAINFLOW))
    p.add_argument(
        "--maybefraction", "-m", type=float, default=DEFAULT_MAYBEFRACTION, metavar="FLOAT",
        help="maximum fraction of flows marked as 'maybe' during matching [{}]".format(
            DEFAULT_MAYBEFRACTION))
    p.add_argument(
        "--alignmaybeflow", type=float, default=DEFAULT_ALIGNMAYBEFLOW, metavar="FLOAT",
        help="fractional flow optional characters during alignment [{}]".format(
            DEFAULT_ALIGNMAYBEFLOW))
    p.add_argument(
        "--alignpseudolength", type=int, default=DEFAULT_ALIGNPSEUDOLENGTH, metavar="INT",
        help="additional pseudo-length of ROIs for scoring [{}]".format(DEFAULT_ALIGNPSEUDOLENGTH))
    p.add_argument(
        "--alignmaxlength", type=int, default=DEFAULT_ALIGNMAXLENGTH, metavar="INT",
        help="maximal length of ROI for scoring [{}]".format(DEFAULT_ALIGNMAXLENGTH))


def main(args):
    analyzer = SffAnalyzer(args)
    analyzer.run()


class SffRead(base.Read):
    def encode(self, args):
        """Return encoded FlowDNA string/list (letters in {ACGTacgt+}) for mapping."""
        read = self.source
        return flowdna.flowdna(
            read.flowvalues, flowchars=read.flowchars,
            maxflow=args.maxflow, maybeflow=args.alignmaybeflow, return_encoded=True)
        # (fdna, fopt, _) = flowdna.flowdna(
        #     read.flowvalues, flowchars=read.flowchars,
        #     maxflow=maxflow, certain=certainflow, maybefraction=maybefraction,
        #     translation=translation, return_lists=True)

    def ix_encode(self):
        fdna = self.encoded
        return flowdna_to_index_map[fdna]


class SffAnalyzer(base.Analyzer):
    READ_FILE_TYPE = "SFF"
    READ_FILE_EXT = ".sff"

    def create_worker(self, config, reportinterval=None):
        if config.args.alignflows:
            return SffFlowAnalyzeWorker(config, reportinterval)
        return SffAnalyzeWorker(config, reportinterval)

    def get_score_matrices(self):
        if self.args.alignflows:
            return flow_matrices
        return flowdna_matrices

    def read_config(self, config_filenames):
        """Obtain configuration information,
        which is read from `config_filenames`, a list of filenames.
        The sequencer key sequence <sffkey> needs to be specified explicitly
        (it must be obtained in advance from the sff file).
        The 454 default key is TCAG.
        """
        # read configfiles, provide sff_key
        # read all config files into the config data structure
        config = utils.read_config_files('', config_filenames)
        # artificially insert the sequencer key sequence
        sffkey = "TCAG"
        if ("KEYS" not in config) or ("KEY" not in config["KEYS"]):
            config.read_dict({"KEYS": {"KEY": sffkey}})
        if "ELEMENTS" not in config:
            config.read_string(CONFIG_STANDARD_ELEMENTS_SFF)
        return config

    def get_elements(self):
        return super().get_elements(self.args.alignpseudolength, True)

    def get_read_files(self):
        filenames = self.filenames
        sff_files = [SffFile(filename) for filename in filenames.reads]
        num_reads = max(1, sum(sff.number_of_reads for sff in sff_files))

        def read_files():
            while sff_files:
                yield sff_files.pop(0)  # remove from list so file can be closed
        return num_reads, read_files()

    def analyze_read_file(self, read_file, writer):
        """Analyze a single sff file `read_file`."""
        elements = self.elements
        args = self.args

        target_element = self.target_element

        logger = logging.getLogger(__name__)
        logger.info("reading file: {}".format(read_file.filename))
        # update key element entry to current SFF key
        sffkey = read_file.key_sequence.upper()
        elements = list(elements)
        ikeys = [el.info.section for el in elements].index("KEYS")
        elements[ikeys] = self.make_element(
            [("KEY", sffkey)], elements[ikeys].info, self.matrices, args.alignpseudolength, True)
        elements = tuple(elements)

        reads = read_file.reads()
        self.process_reads(reads, elements, target_element, writer)


class SffAnalyzeWorker(base.AnalyzeWorker):
    def create_read(self, t, source, args):
        return SffRead(t, source, args)

    def get_score_matrix(self, direction):
        if self.target_element.info.bis == Bis.NONE:
            return self.matrices.standard
        if direction == Directions.FWD:
            return self.matrices.bisulfiteCT
        if direction == Directions.REV:
            return self.matrices.bisulfiteGA
        raise ValueError("align with bisulfite requires direction")

    def align(self, genomic, read, direction=None, cutprefix=40):
        """Align genomic sequence `genomic` to read `read`.
        Return pair `((score, score_possible), alignment)`.
        """
        args = self.args

        score_matrix = self.get_score_matrix(direction)
        fdna = read.encoded

        (score, score_possible, alignment) = align_genomic_to_flowdna(
            genomic, fdna, cutprefix,
            args.alignthreshold, args.alignmaxlength, args.alignpseudolength,
            score_matrix)
        return (score, score_possible), alignment


class SffFlowAnalyzeWorker(base.AnalyzeWorker):
    def create_read(self, t, source, args):
        return SffRead(t, source, args)

    def get_score_matrix(self, direction):
        if self.target_element.info.bis == Bis.NONE:
            return self.standard
        if direction == Directions.FWD:
            return self.matrices.bisulfiteCT
        if direction == Directions.REV:
            return self.matrices.bisulfiteGA
        raise ValueError("align with bisulfite requires direction")

    def align(self, genomic, read, direction=None, cutprefix=40):
        """Align genomic sequence `genomic` to read `read`, using its flows.
        Return pair `((score, score_possible), alignment)`.
        """
        flows = read.source.flowvalues
        flowchars = read.source.flowchars
        score_matrix = self.get_score_matrix(direction)

        (score, score_possible, alignment) = align_genomic_to_flows(
            genomic, flows, flowchars, cutprefix,
            score_matrix, suppress_gaps=True)
        return (score, score_possible), alignment
