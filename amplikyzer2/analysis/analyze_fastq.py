# amplikyzer2.analysis module
# implements the 'analyze-fastq' subcommand
# (c) 2011--2012 Sven Rahmann

"""
Analyze (MiSeq) .fastq files.
Split each dna sequence in the .fastq files into
predefined parts (like mid, tag, primer, region of interest)
and align with given genomic reference sequence.
Write an .akzr file containing the analysis results and alignments.

Use the 'statistics' subcommand to obtain a summary of the results.
Use the 'align' subcommand to format the alignments for output.
Use the 'methylation' subcommand to do methylation analysis on the alignments.
"""

from itertools import chain
from functools import partial
from collections import OrderedDict, namedtuple
import os.path
from enum import IntEnum, unique
import re
# from multiprocessing import Process, Queue
import logging

import numpy as np
from numba import njit

from .. import utils
from ..alphabets import encode
from ..fastq import FastqFile
from .common import Directions, opposite_direction, Special, Bis
from .output import FastqAkzrWriter
from .scoring.common import ScoringMatrices
from .scoring.dna import (
    ScoreMatrixDNA5, genomic_to_dna5_index_map,
    align_genomic_to_genomic, align_genomic_pair_to_genomic, qual_dtype)
from .base import (
    buildparser as buildparser_common,
    Element, ElementInfo, Read, Analyzer, AnalyzeWorker)


CONFIG_STANDARD_ELEMENTS_FASTQ = """
[ELEMENTS]
# syntax is:
# order = ( name,  config.SECTIONHEADER,  rc_{no|ok},  bis_{none|tagged|all},
#           startpos,  tolstartpos|-1,  threshold,
#           required=0|1,  special_{none|tag|target|miseq:{file|read}:...} )
#1 = mid,   MIDS, rc_no, bis_none,    0, -1,  0.75,   0, special_index
#2 = var,   VARS, rc_no, bis_none,    0,  1,  0.75,   0, special_none
1 = mid,   MIDS, rc_no, bis_none,    0,  0,  0.00,   0, special_miseq:file:sample_name
3 = tag,   TAGS, rc_no, bis_none,    0,  7,  0.65,   0, special_tag
4 = locus, LOCI, rc_ok, bis_tagged, 17, -1,  0.65,   1, special_target
"""

ARG_TRIM_META = ("QUAL", "WIN", "BAD", "LEN")
DEFAULT_ARG_TRIM = (20, 6, 3, 50)
ARG_SCORING_META = ("MATCH", "MISMATCH", "INSERT", "DELETE")
DEFAULT_ARG_SCORING = (10, -15, -45, -45)  # match=10; mis=-1.5*match; ins=3*mis; del=ins
ARG_MERGEQUAL_META = ("DELTA", "INSERT")
DEFAULT_ARG_MERGEQUAL = (5, 25)


def buildparser(p):
    buildparser_common(p)
    # from inspect import cleandoc as dedent
    p.add_argument(
        "--reads", "--fastq", nargs="+", metavar="FILE",
        default=[''.join(('*', ext, gz)) for gz in ('.gz', '') for ext in ('.fq', '.fastq')],
        help="FASTQ file(s) to analyze")
    p.add_argument(
        "--trim", metavar=ARG_TRIM_META, type=int, nargs=len(ARG_TRIM_META),
        default=DEFAULT_ARG_TRIM,
        help=("trim reads if BAD out of WIN consecutive bases have quality < QUAL."
              " Discard trimmed reads shorter than LEN [{} = {}]".format(
                  " ".join(ARG_TRIM_META), " ".join(map(str, DEFAULT_ARG_TRIM)))))
    p.add_argument(
        "--numreads", type=int, default=0, metavar="INT",
        help="estimate of total number of (paired) reads, used to display remaining time")
    p.add_argument(
        "--scoring", nargs=len(ARG_SCORING_META), type=int, metavar=ARG_SCORING_META,
        default=DEFAULT_ARG_SCORING,
        help=("scoring constants used for alignment and mapping [{} = {}]".format(
                  " ".join(ARG_SCORING_META), " ".join(map(str, DEFAULT_ARG_SCORING)))))
    p.add_argument(
        "--mergequal", nargs=len(ARG_MERGEQUAL_META), type=int, metavar=ARG_MERGEQUAL_META,
        default=DEFAULT_ARG_MERGEQUAL,
        help=("when merging read pairs, replace unagreeing bases by 'N' if their qualities differ"
              " less than DELTA; don't insert base only present in one read inside overlap if its"
              " quality is less than INSERT [{} = {}]".format(
                  " ".join(ARG_MERGEQUAL_META), " ".join(map(str, DEFAULT_ARG_MERGEQUAL)))))


def main(args):
    analyzer = FastqAnalyzer(args)
    analyzer.run()


_MISEQ_SPECIAL_PREFIX = "special_miseq"
_MISEQ_SPECIAL_INDEX = "special_index"


class MiSeqSpecial:
    @unique
    class Source(utils.ConfigEnum):
        FILE = "file"
        READ = "read"

    class Pattern:
        FILE_NAME_PREFIX = re.compile(
            r"""
            (?P<sample_prefix>
                (?P<sample_id>
                    (?P<sample_name>[^/]*)
                    _
                    (?P<sample_number>[^_/]*)
                )
                _
                L[0-9]{3,3}
            )
            """, re.VERBOSE)
        FILE_NAME = re.compile(
            FILE_NAME_PREFIX.pattern +
            r"""
            _
            (?P<read_type>[RI])
            (?P<read_num>[12])
            _001
            \.(?:fastq|fq)(?:.gz)?
            """, re.VERBOSE)

        READ_NAME_ID = re.compile(
            r"""
            (?P<read_id>
                (?P<instrument>[^:]*):
                (?P<run_id>[^:]*):
                (?P<flow_cell_id>[^:]*):
                (?P<lane>[^:]*):
                (?P<tile>[^:]*):
                (?P<x>[^:]*):
                (?P<y>[^: ]*)
            )
            """, re.VERBOSE)
        READ_NAME = re.compile(
            READ_NAME_ID.pattern +
            r"""
            [ ]
            (?P<read_num>[^:]*):
            (?P<filter_flag>[^:]*):
            0:
            (?P<sample_number>[^:]*)
            """, re.VERBOSE)

    _source_patterns = OrderedDict([
        (Source.FILE, Pattern.FILE_NAME_PREFIX),
        (Source.READ, Pattern.READ_NAME_ID),
    ])

    _Enum = unique(utils.ConfigEnum(
        'MiSeqSpecial',
        [  # ("MISEQ_FILE_SAMPLE_PREFIX", "special_miseq:file:sample_prefix"), etc.
            ("_".join((_MISEQ_SPECIAL_PREFIX, source.value, group)).upper(),
             ":".join((_MISEQ_SPECIAL_PREFIX, source.value, group)))
            for source, pattern in _source_patterns.items()
            for group in sorted(pattern.groupindex, key=lambda g:pattern.groupindex[g])
        ] +
        [("INDEX", _MISEQ_SPECIAL_INDEX)] +
        [(e.name, e.value) for e in Special]))  # NOTE: Special.* != _Enum.* (enums!)

    INDEX = _Enum.INDEX

    def __init__(self, source, group):
        source = self.Source.from_value(source)
        self.source = source
        self.pattern = self._source_patterns[source]
        self.group = group
        assert self.group in self.pattern.groupindex

    @classmethod
    def from_string(cls, string):
        x = cls._Enum.from_value(string)
        if x == cls.INDEX:
            return cls.INDEX
        prefix = _MISEQ_SPECIAL_PREFIX + ":"
        if string.startswith(prefix):
            source, group = string[len(prefix):].split(":")
            return cls(source, group)
        return Special.from_value(string)


class FastqElementInfo(ElementInfo):
    __slots__ = ()
    _field_convs = tuple((MiSeqSpecial.from_string if field == "special" else conv)
                         for conv, field in zip(ElementInfo._field_convs, ElementInfo._fields))


class EncodedRead(namedtuple('EncodedRead', ['bases', 'qual'])):
    __slots__ = ()

    def __new__(cls, bases, qual):
        assert len(bases) == len(qual)
        return super().__new__(cls, bases, qual)

    def __len__(self):
        return len(self.bases)


class FastqRead(Read):
    def encode(self, args):
        args_trim = args.trim
        enc_read_set = ReadSet()
        for read_type, reads_of_type in enumerate(self.source.reads):
            for read_num, read in enumerate(reads_of_type):
                if read is None:
                    continue
                args_trim_ = args_trim if (read_type == ReadType.SEQ) else None
                enc_read = self._encode_single_read(read, args_trim_)
                enc_read_set.add(read_type, read_num, enc_read)
        return enc_read_set

    def ix_encode(self):
        ix_read_set = ReadSet()
        for read_type, reads_of_type in enumerate(self.encoded.reads):
            for read_num, read in enumerate(reads_of_type):
                if read is None:
                    continue
                ix_read = EncodedRead(genomic_to_dna5_index_map[read.bases], read.qual.copy())
                ix_read_set.add(read_type, read_num, ix_read)
        return ix_read_set

    def _encode_single_read(self, read, args_trim):
        """Return encoded genomic string with its base quality values for mapping."""
        qual = np.array(read.qual, dtype=qual_dtype)
        bases = read.bases
        if args_trim is not None:
            (min_qual, window_len, max_bad, min_len) = args_trim
            start, end = self._trim_read(qual, min_qual, window_len, max_bad)
            assert end <= len(bases)
            if end - start < min_len:
                end = start
            bases = bases[start:end]
            qual = qual[start:end]
        enc_bases = encode(bases)
        return EncodedRead(enc_bases, qual)

    @staticmethod
    @njit(cache=True)
    def _trim_read(qual, min_qual, window_len, max_bad):
        n = len(qual)
        pos = 0
        for pos in range(n):
            if qual[pos] >= min_qual:
                break
        start = pos
        bad_sum = 0
        for pos in range(pos, pos + window_len-1):
            bad_sum += (qual[pos] < min_qual)
        for pos in range(pos + window_len-1, n):
            bad_sum += (qual[pos] < min_qual)
            if bad_sum > max_bad:
                return start, pos - window_len+1
            bad_sum -= (qual[pos - window_len+1] < min_qual)
        return start, n


class ReadType(IntEnum):
    SEQ = 0
    IDX = 1

    @staticmethod
    def new_set():
        num_reads = 2
        seq = [None] * num_reads
        idx = [None] * num_reads
        return [seq, idx]


class ReadSet:
    def __init__(self, name=None):
        self.reads = ReadType.new_set()
        self.seq_reads, self.idx_reads = self.reads
        if name is not None:
            self.name = name

    def __len__(self):
        return sum(1 for r in chain.from_iterable(self.reads) if r)

    def add(self, read_type, read_num, read):
        assert self.reads[read_type][read_num] is None
        self.reads[read_type][read_num] = read


class ReadFileSet:
    def __init__(self, path):
        self.filename = path
        self.files = ReadType.new_set()
        self.seq_files, self.idx_files = self.files

    def add(self, read_type, read_num, file_name):
        if self.files[read_type][read_num] is not None:
            return  # raise ValueError() ??
        self.files[read_type][read_num] = FastqFile(file_name)

    def __len__(self):
        return sum(1 for _ in utils.filter_is_not(chain.from_iterable(self.files)))

    def get_reads(self):
        filenames = [file.filename for file in chain.from_iterable(self.files) if file is not None]
        num_files = len(self)
        logger = logging.getLogger(__name__)
        logger.info("reading file{}: {}".format("s" if len(filenames) > 1 else "", filenames))

        max_cache_size = 1000
        cache = OrderedDict()
        p = MiSeqSpecial.Pattern.READ_NAME
        all_reads = [[(f.reads() if f is not None else iter(())) for f in fs] for fs in self.files]
        while True:
            extracted_reads = 0
            for read_type, reads_of_type in zip(ReadType, all_reads):
                for read_num, reads in enumerate(reads_of_type):
                    try:
                        read = next(reads)
                    except StopIteration:
                        continue
                    extracted_reads += 1
                    match = p.fullmatch(read.name)
                    # if pattern doesn't match, assume no MiSeq-read, use 'read.name'
                    read_id = match.group("read_id") if match else read.name
                    # TODO: maybe check 'read_num == match.group("read_num")'?
                    if read_id not in cache:
                        cache[read_id] = ReadSet(read_id)
                    read_set = cache[read_id]
                    read_set.add(read_type, read_num, read)
                    if len(read_set) == num_files:
                        yield read_set
                        del cache[read_id]
            if extracted_reads == 0:
                max_cache_size = 1
            while len(cache) >= max_cache_size:
                read_id, read_set = next(iter(cache.items()))
                if any(r is not None for r in read_set.seq_reads):
                    yield read_set
                # else: TODO: log/info
                del cache[read_id]
            if extracted_reads == 0:
                break


class FastqAnalyzer(Analyzer):
    READ_FILE_TYPE = "FASTQ"
    READ_FILE_EXT = ".fastq"

    def create_writer(self, fnout, force, omit_states):
        return FastqAkzrWriter(fnout, force, omit_states)

    def create_worker(self, config, reportinterval=None):
        return FastqAnalyzeWorker(config, reportinterval)

    def get_score_matrices(self):
        match, mismatch, insert, delete = self.args.scoring
        make_matrix = partial(ScoreMatrixDNA5, match, mismatch, delete, insert)
        return ScoringMatrices(
            make_matrix(bisulfite=0),
            make_matrix(bisulfite=1),
            make_matrix(bisulfite=-1))

    def read_config(self, config_filenames):
        config = utils.read_config_files('', config_filenames)
        if "ELEMENTS" not in config:
            config.read_string(CONFIG_STANDARD_ELEMENTS_FASTQ)
        return config

    def get_element_info(self, string):
        return FastqElementInfo.from_string(string)

    def get_elements(self):
        return super().get_elements(0, None)

    def make_element(self, entries, info, matrices, pseudolength, isflowdna):
        special = info.special
        if special == MiSeqSpecial.INDEX:
            return MiSeqIndexElement(entries, info, matrices, pseudolength, isflowdna)
        if isinstance(special, MiSeqSpecial):
            if special.source == MiSeqSpecial.Source.FILE:
                return MiSeqFileMetaElement(entries, info, matrices, pseudolength, isflowdna)
            return MiSeqReadMetaElement(entries, info, matrices, pseudolength, isflowdna)
        return super().make_element(entries, info, matrices, pseudolength, isflowdna)

    def get_common_input_name(self, read_file_names):
        read_file_name_set = set()
        p = MiSeqSpecial.Pattern.FILE_NAME
        for fname in read_file_names:
            dirname, basename = os.path.split(os.path.normpath(fname))
            match = p.fullmatch(basename)
            if match:
                read_file_name_set.add(match.group("sample_id"))
            else:
                read_file_name_set.add(basename)
        return super().get_common_input_name(sorted(read_file_name_set))

    def get_read_files(self):
        filenames = self.filenames
        args = self.args
        need_index_files = any(
            element.info.special == MiSeqSpecial.INDEX for element in self.elements)

        read_type_from_string = {"R": ReadType.SEQ, "I": ReadType.IDX}
        read_file_sets = OrderedDict()
        p = MiSeqSpecial.Pattern.FILE_NAME
        for fname in filenames.reads:
            dirname, basename = os.path.split(os.path.normpath(fname))
            match = p.fullmatch(basename)
            if match:
                read_type = read_type_from_string[match.group("read_type")]
                if not need_index_files and (read_type == ReadType.IDX):
                    continue
                read_num = int(match.group("read_num")) - 1
                path = os.path.join(dirname, match.group("sample_prefix"))
            else:
                read_type = ReadType.SEQ
                read_num = 0
                path = os.path.join(dirname, basename)
            if path not in read_file_sets:
                read_file_sets[path] = ReadFileSet(path)
            read_file_sets[path].add(read_type, read_num, fname)
        return max(1, args.numreads), read_file_sets.values()

    def analyze_read_file(self, read_file_set, writer):
        reads = read_file_set.get_reads()
        elements = self.elements
        for element in elements:
            if isinstance(element, MiSeqFileMetaElement):
                element.update(read_file_set.filename)
                # NOTE: `element` (and therefore `self.elements`) is modified in-place.
                #       If at some point in time the code is altered to call `analyze_read_file`
                #       in parallel for multiple files, this has to be changed!
        target_element = self.target_element
        self.process_reads(reads, elements, target_element, writer)


class FastqAnalyzeWorker(AnalyzeWorker):
    def create_read(self, t, source, args):
        return FastqRead(t, source, args)

    def map_element(self, read, direction, element):
        if (element.info.special == MiSeqSpecial.INDEX) or isinstance(element, MiSeqMetaElement):
            return element.match(read)
        matches = []
        for i, seq_read in enumerate(utils.filter_is_not(read.ix_encoded.seq_reads)):
            if not seq_read:
                matches.append([])
                continue
            dir_ = opposite_direction[direction] if (i > 0) else direction
            matches.append(element.match(seq_read.bases, dir_))
        return matches

    def get_score_matrix(self, direction):
        if self.target_element.info.bis == Bis.NONE:
            return self.matrices.standard, self.matrices.standard
        if direction == Directions.FWD:
            return self.matrices.bisulfiteCT, self.matrices.bisulfiteGA
        if direction == Directions.REV:
            return self.matrices.bisulfiteGA, self.matrices.bisulfiteCT
        raise ValueError("align with bisulfite requires direction")

    def align(self, genomic, read, direction=None):
        """Align genomic sequence `genomic` to read `read`.
        Return pair `((score, score_possible), alignment)`.
        """
        args = self.args

        score_matrix1, score_matrix2 = self.get_score_matrix(direction)

        enc_seq_read = list(utils.filter_is_not(read.encoded.seq_reads))
        if len(enc_seq_read) == 1:
            read1, = enc_seq_read
            (score, score_possible, alignment) = align_genomic_to_genomic(
                genomic, read1, args.alignthreshold,
                score_matrix1)
        else:
            (mismatch_qual_delta_threshold, insert_qual_threshold) = args.mergequal
            read1, read2 = enc_seq_read
            (score, score_possible, alignment) = align_genomic_pair_to_genomic(
                genomic, read1, read2, args.alignthreshold,
                score_matrix1, score_matrix2,
                mismatch_qual_delta_threshold, insert_qual_threshold)
        return (score, score_possible), alignment


class MiSeqMetaElement(Element):
    def __init__(self, entries, info, matrices, pseudolength, isflowdna):
        self.aliases = dict()
        super().__init__(entries, info, matrices, pseudolength, isflowdna)

    def _parse_entries(self, entries):
        aliases = dict()
        for name, seq in entries:
            if seq not in aliases:
                aliases[seq] = name
            # else:  # TODO: raise error / log warning; should be done for every `base.Element`!
            #    raise ConfigFormatError("Duplicate entry for '{}'".format(seq))
        self.aliases = aliases
        return [], []


class MiSeqFileMetaElement(MiSeqMetaElement):
    def __init__(self, entries, info, matrices, pseudolength, isflowdna):
        super().__init__(entries, info, matrices, pseudolength, isflowdna)
        self.pattern = MiSeqSpecial.Pattern.FILE_NAME_PREFIX
        self.group = self.info.special.group
        assert self.group in self.pattern.groupindex
        self.value = None

    def update(self, path):
        basename = os.path.basename(path)
        m = self.pattern.match(basename)
        if not m:
            self.value = None
        else:
            value = m.group(self.group)
            value = self.aliases.get(value, value)
            self.value = value

    def match(self, read):
        # TODO: Don't return `str` but instead a type less dependent on output
        value = self.value
        return value or "?"  # return "?" if value is None or value == ""


class MiSeqReadMetaElement(MiSeqMetaElement):
    def __init__(self, entries, info, matrices, pseudolength, isflowdna):
        super().__init__(entries, info, matrices, pseudolength, isflowdna)
        self.pattern = MiSeqSpecial.Pattern.READ_NAME_ID
        self.group = self.info.special.group
        assert self.group in self.pattern.groupindex

    def match(self, read):
        # TODO: Don't return `str` but instead a type less dependent on output
        value = None
        for seq in utils.filter_is_not(read.source.seq_reads):
            m = self.pattern.match(seq.name)
            if m:
                value = m.group(self.group)
                break
        value = self.aliases.get(value, value)
        return value or "?"  # return "?" if value is None or value == ""


class MiSeqIndexElement(Element):
    def __init__(self, entries, info, matrices, pseudolength, isflowdna):
        self.aliases = dict()
        super().__init__(entries, info, matrices, pseudolength, isflowdna)

    def _parse_entries(self, entries):
        aliases = dict()
        filtered_entries = []
        for name, seq in entries:
            if seq.startswith(">"):
                aliases[seq[1:]] = name
            else:
                filtered_entries.append((name, seq))
        entries = filtered_entries
        self.aliases = aliases
        names, seqs = zip(*entries) if entries else ([], [])
        return names, seqs

    def match(self, read):
        # TODO: Don't return `str` but instead a type less dependent on output
        p = MiSeqSpecial.Pattern.READ_NAME
        source = read.source
        ix_encoded = read.ix_encoded

        aliases = self.aliases
        ids = []
        mapped = []
        for i, (seq, idx) in enumerate(zip(source.seq_reads, source.idx_reads)):
            if (seq is None) and (idx is None):
                continue
            id_i = "0"
            # try to extract id from sequence id (from seq or index read)
            for r in (seq, idx):
                if (id_i == "0") and (r is not None):
                    m = p.fullmatch(r.name)
                    if m:
                        id_i = m.group("sample_number")
            mapped_i = False
            if id_i != "0":
                # use a direct mapping if one is given
                if id_i in aliases:
                    mapped_i = True
                    id_i = aliases[id_i]
            else:
                # try matching only if genomic sequences for mids are given
                # if an index read is given, match its sequence
                id_i = ""
                if self.names and ix_encoded.idx_reads[i]:
                    genomic_index_i = ix_encoded.idx_reads[i].bases
                    if genomic_index_i:
                        matches = super().match(genomic_index_i, Directions.FWD)
                        if matches:
                            # just use the best match if any
                            mapped_i = True
                            id_i = self.names[matches[0][-1]]
            ids.append(id_i)
            mapped.append(mapped_i)
        if all(m == ids[0] for m in ids[1:]):
            ids = [ids[0]]  # assume single index
        ids = [m if m else "?" for m in ids]
        mid_str = "+".join(ids)
        if (len(ids) == 1) and all(mapped):
            showdesc = mid_str
        elif (not aliases) and (not self.names) and ("?" not in ids):
            showdesc = mid_str
        elif mid_str in aliases:
            showdesc = aliases[mid_str]
        else:
            showdesc = "? {}".format(mid_str)
        return showdesc
