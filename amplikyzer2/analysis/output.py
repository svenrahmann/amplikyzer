import sys
import gzip
from functools import partial
from itertools import zip_longest

from ..exceptions import AkzrOutputError
from ..constants import TAGSUFFIX_SEP
from ..utils import filter_is_not
from ..alphabets import decode
from .common import State, Match, Bis, Special, direction_to_tag


class AkzrWriter:
    alignment_entries = [
        "scores",
        "direction",
        "ROI",
        "forward_primer",
        "reverse_primer",
        "genomic",
        "read",
    ]

    def __init__(self, fnout, force, omit_states):
        self.omit_states = set(omit_states)
        if fnout == "-":
            self._fout = sys.stdout
            self.close = lambda: None
        else:
            open_ = gzip.open if fnout.endswith(".gz") else open
            mode = "wt" if force else "xt"
            self._fout = open_(fnout, mode)
            self.close = self._fout.close
        self._print = partial(print, file=self._fout, end="\n")

    def create_result_formatter(self, elements, target_element):
        return ResultFormatter(elements, target_element, self.omit_states)

    def __enter__(self):
        return self  # file already opened in `__init__`

    def __exit__(self, exc_type, exc, exc_tb):
        self.close()
        return False

    def write_header(self, filenames, read_file_type, args, elements):
        arg_entries = ["{} {}".format(arg, getattr(args, arg)) for arg in dir(args)
                       if (not arg.startswith("_")) and (arg != "func")]
        sections = [
            ("@{}".format(read_file_type), filenames.reads),
            ("@CONF", filenames.config),
            ("@ARG", arg_entries),
            "",
            ("@ELEMENT", [element.info.name for element in elements]),
            ("@ALIGNMENT", self.alignment_entries),
            "",
        ]
        self._write_header_sections(sections)
        self.write_header = self._write_header_2
        self.write_entry = self._write_entry_2

    def _write_header_sections(self, sections):
        for section in sections:
            if isinstance(section, str):
                self._print(section)
                continue
            typ, entries = section
            print_entry = partial(self._print, typ)
            for entry in entries:
                print_entry(entry)

    def _write_header_2(self, filenames, read_file_type, args, elements):
        raise AkzrOutputError("Header already written")

    def write_entry(self, state, result):
        raise AkzrOutputError("Header not yet written")

    def _write_entry_2(self, state, result):
        if state != State.SKIPPED:
            self._print(result)


class ResultFormatter:
    _NO_MATCH = Match(0, 0, 0, 0, None)

    def __init__(self, elements, target_element, omit_states):
        self.elements = elements
        self.target_element = target_element
        self.omit_states = omit_states

    def format(self, read, state, result):
        if str(state) in self.omit_states:
            return ["# {} >{} {}".format(state, read.source.name, read.t)]
        output = self.format_result(read, state, result)
        output.append("")
        return output

    def format_result(self, read, state, result):
        (mappings, targets, align_result) = result
        output_map, comments_map = self.format_mappings(mappings)
        output_align, comments_align = self.format_align_result(targets, align_result, state)
        comments = comments_map + comments_align
        output = [">{} {}".format(read.source.name, read.t)]
        output.extend(output_map)
        output.append("# {}".format("; ".join(comments)))
        output.extend(output_align)
        return output

    def format_mappings(self, mappings):
        comments = []
        if mappings is None:
            output = ["?"] * len(self.elements)
            return output, comments
        output = []
        has_tag = None
        for element, matches in zip(self.elements, mappings):
            output.append(self.format_matches(element, matches))
            if (element.info.special == Special.TAG):
                has_tag = has_tag or any(matches)
            if element.info.required and not any(matches):
                comments.append("required '{}' not found".format(element.info.name))
        if (has_tag is not None) and not has_tag:
            comments.append("no tag")
        return output, comments

    @classmethod
    def format_matches(cls, element, matches):
        if isinstance(matches, str):
            return matches  # `MiSeq..Element.match` currently return a plain string
        if not (matches and any(matches)):
            matches = [[cls._NO_MATCH]]
        format_match_list = partial(cls.format_match_list, element)
        match_to_strings = partial(cls.match_to_strings, element)
        per_read_descs = map(format_match_list, matches)
        zipped_descs = zip_longest(*per_read_descs, fillvalue=match_to_strings(cls._NO_MATCH))
        showdescs = [" ".join("|".join(z) for z in zip(*x)) for x in zipped_descs]
        showdesc = ", ".join(showdescs)
        return showdesc

    @classmethod
    def format_match_list(cls, element, match_list):
        return map(partial(cls.match_to_strings, element), match_list)

    @classmethod
    def match_to_strings(cls, element, match):
        name = cls.get_target_name(element, match)
        if match.score_possible < 1:
            score_relative = "100%"
        else:
            score_relative = "{:.0%}".format(match.score / match.score_possible)
        return (name, score_relative, str(match.score), str(match.score_possible))

    @staticmethod
    def get_target_name(element, match):
        target = match.target
        if target is None:
            return "?"
        (direction, i) = target
        name = element.names[i]
        if element.info.bis == Bis.NONE:
            return name
        return name + TAGSUFFIX_SEP + direction_to_tag[direction]

    def format_align_result(self, targets, align_result, state):
        (target, score, alignment, comments) = self.get_alignment(targets, align_result, state)
        # compute score percentage
        score, score_possible = score
        score_percentage = min(score / max(1, score_possible), 0.99)
        # output scores, direction, target-roi-name, primers, genomic, read
        if target is None:
            direction = "?"
            name = "?"
            primers = ("?", "?")
        else:
            (direction, index) = target
            direction = direction_to_tag[direction]
            name = self.target_element.names[index]
            primers = self.target_element.primers[index]
        forward_primer, reverse_primer = (p if p else "?" for p in primers)
        output = []
        output.append("{:.0%} {} {}".format(score_percentage, score, score_possible))
        output.append(direction)
        output.append(name)
        output.append(forward_primer)
        output.append(reverse_primer)
        output.extend(alignment)
        return output, comments

    def get_alignment(self, targets, align_result, state):
        comments = []
        target = None
        score = (0, 0)
        if not self.target_element:
            alignment = ("? N/A", "? N/A")
        elif not targets:
            alignment = ("? no_targets", "? no_targets")
        else:
            comments.append("aligning to {} targets".format(len(targets)))
            if align_result is None:
                alignment = ("? no_good_alignment", "? no_good_alignment")
            else:
                (target, score, alignment) = align_result
                alignment = tuple(map(decode, alignment))
                if state == State.INSUFFICIENT_COVERAGE:
                    alignment = tuple("? " + s for s in alignment)
        return (target, score, alignment, comments)


class FastqAkzrWriter(AkzrWriter):
    # should be @MISC or the like; @ALIGNMENT for backwards compatibility
    alignment_entries = AkzrWriter.alignment_entries + ["filter"]

    def create_result_formatter(self, elements, target_element):
        return FastqResultFormatter(elements, target_element, self.omit_states)


class FastqResultFormatter(ResultFormatter):
    def format_result(self, read, state, result):
        output = super().format_result(read, state, result)

        def filtered(r):
            return "-" if (len(r) == 0) else "+"
        enc_seq_read = filter_is_not(read.encoded.seq_reads, None)
        output.append("|".join(filtered(r) for r in enc_seq_read))
        return output
