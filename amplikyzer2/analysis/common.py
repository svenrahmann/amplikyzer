from enum import Enum, unique
from collections import namedtuple

from ..constants import TAG_FWD, TAG_REV
from ..utils import ConfigEnum

DEFAULT_ANALYSIS_OUTPUT_NAME = "analysis"


@unique
class Rc(ConfigEnum):
    NO = "rc_no"
    OK = "rc_ok"


@unique
class Bis(ConfigEnum):
    NONE = "bis_none"
    TAGGED = "bis_tagged"
    ALL = "bis_all"


@unique
class Special(ConfigEnum):
    NONE = "special_none"
    TAG = "special_tag"
    TARGET = "special_target"


class State(Enum):
    FILTERED = "filtered"
    UNMAPPED = "unmapped"
    UNALIGNED = "unaligned"
    ALIGNED = "aligned"
    INSUFFICIENT_COVERAGE = "badcoverage"
    SKIPPED = "skipped"

    def __str__(self):
        return str(self.value)


# NOTE:'namedtuple' since something is slower when `IntEnum` is used...
Directions = namedtuple("Directions", ["ALL", "FWD", "REV"])(-1, 0, 1)
tag_to_direction = {TAG_FWD: Directions.FWD, TAG_REV: Directions.REV}
direction_to_tag = {Directions.FWD: TAG_FWD, Directions.REV: TAG_REV}

opposite_direction = {
    Directions.ALL: Directions.ALL,
    Directions.FWD: Directions.REV,
    Directions.REV: Directions.FWD,
}


Match = namedtuple('Match', ['score', 'score_possible', 'column', 'row', 'target'])
