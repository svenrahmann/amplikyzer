from sys import stdout
from itertools import product
from collections import namedtuple

import numpy as np
from numba import njit, generated_jit

from ...alphabets import (
    GENOMIC, DNA5, genomic_to_dna5, SENTINEL_STR, encode, make_map, revcomp, N, GAP, VOID)
from .common import (
    traceback, TRACE, make_compute_dp_matrix, make_match_reference_to_read, make_compute_dp,
    get_score_threshold, substitue_outer_gaps_with_void)


_genomic_to_dna5_index_dict = {x: i for i, x in enumerate(DNA5 + SENTINEL_STR)}
_genomic_to_dna5_index_dict.update(
    {x: _genomic_to_dna5_index_dict[genomic_to_dna5(x)] for x in GENOMIC})


def genomics_to_dna5_indices(genomics, d=_genomic_to_dna5_index_dict):
    """convert a genomic sequence to its score matrix indices"""
    return [d[x] for x in genomics]


genomic_to_dna5_index_map = make_map(
    zip(encode(GENOMIC), genomics_to_dna5_indices(GENOMIC)),
    fill=genomics_to_dna5_indices(SENTINEL_STR)[0])
dna5_index_to_genomic_map = make_map(
    zip(genomics_to_dna5_indices(DNA5), encode(DNA5)),
    fill=encode(SENTINEL_STR)[0])


@njit(cache=True)
def genomic_to_dna5_index(genomic):
    return genomic_to_dna5_index_map[genomic]


@njit(cache=True)
def dna5_index_to_genomic(index):
    return dna5_index_to_genomic_map[index]


class ScoreMatrixDNA5:
    """Score matrix of size |DNA5| x |DNA5| = 5 x 5.
    User-controlled initialization parameters:
      match = match score
      mismatch = mismatch score
      bisulfite in {0,1,-1}: adjust score matrix for bisulfite treatment:
          0: no adjustment, 1: C->T substitutions ok, 2: G->A substitutions ok
    Attributes:
      self.maxscore - maximum score of this matrix
      self.minscore - mimimum score of this matrix
      self.score - score matrix as a dict of dicts,
          indexed as score[flow][dna], e.g., score["C"]["G"]
      self.matrix - score matrix as a matrix, indexed matrix[1][2],
          use genomics_to_dna5_indices to compute indices.
    Methods:
      self.insflow(fl,fr,g) - score for inserting genomic g between flowdna fl, fr
      self.delflow(f,gl,gr) - score for deleting flow f between genomic gl, gr
    """

    def __init__(
            self, score_match=10, score_mismatch=None, score_delete=None, score_insert=None,
            bisulfite=0):
        """Instantiate a score matrix with the given parameters.

        score_match: final match score, determines scaling constant.
        bisulfite: 0 = no, 1=C->T, 2=G->A.
        """
        if score_mismatch is None:
            score_mismatch = int(round(-1.5 * score_match))
        if score_delete is None:
            score_delete = 3 * score_mismatch
        if score_insert is None:
            score_insert = score_delete

        if bisulfite == 0:
            self.bisulfite_conversion = {}
        elif bisulfite == 1:  # genomic C -> flow T is ok
            self.bisulfite_conversion = {"C": "CT"}
        elif bisulfite == -1:  # genomic G -> flow A is ok
            self.bisulfite_conversion = {"G": "GA"}
        else:
            raise ValueError("bisulfite parameter must be in {0,1,-1}.")
        # compute score values
        self.score_match = score_match
        self.score_mismatch = score_mismatch
        self.score_delete = score_delete
        self.score_insert = score_insert
        self.score = self.compute_scores()
        (self.matrix, self.insflow_array, self.delflow_array) = self._compute_arrays()
        values = self.matrix
        self.maxscore = values.max()
        self.minscore = values.min()
        # self.show(file=stdout)

    def show(self, file=stdout):
        for f, row in zip(GENOMIC, self.matrix):
            print(f, row, file=file)
        print("min={}, max={}, range={}".format(
            self.minscore, self.maxscore, self.maxscore - self.minscore))

    def compute_scores(self):
        """Compute and return score matrix dictionary."""
        # dict of dict of scores (score matrix as dict)
        bisulfite_conversion = self.bisulfite_conversion
        score = dict()
        for read_char in DNA5:
            d = score[read_char] = dict()
            for ref_char in DNA5:
                if read_char in bisulfite_conversion.get(ref_char, ref_char):
                    d[ref_char] = self.score_match
                elif "N" in {read_char, ref_char}:
                    # TODO: "N" in read is a sequencing error; should this count as a mismatch?
                    d[ref_char] = self.score_match // 2
                else:
                    d[ref_char] = self.score_mismatch
        return score

    def _compute_arrays(self):
        """Return for indices f, fl, fr and genomic indices g, gl, gr.
         - matrix[f][g] with matching scores;
         - insflow_array[fl][fr][g] with insertion scores;
         - delflow_array[f][gl][gr] with deletion scores;
        """
        def generate_array(func, *strings):
            m = np.asarray([func(*t) for t in product(*strings)], dtype=np.int32)
            return m.reshape([len(s) for s in strings])

        genomics = DNA5
        score = generate_array(lambda f, g: self.score[f][g], genomics, genomics)
        insflow = self.score_insert
        delflow = self.score_delete
        return score, insflow, delflow

    def insflow(self, fl, fr, g):
        """Penalty for inserting g between fl and fr."""
        if fr == SENTINEL_STR:
            return 0  # read exhausted -- do not penalize!
        return self.score_insert

    def delflow(self, f, gl, gr):
        """Penalty for deleting f between genomic gl, gr."""
        return self.score_delete


###################################################################################################
# dna alignment and scoring

def align_genomic_to_genomic(genomic_ref, genomic_read, align_threshold, score_matrix):
    """Align `genomic_ref` to `genomic_read` using score matrix
    `(matchflow, insflow, delflow, maxscore)`.
    Return `(score, column_index, alignment)`, where `alignment` is a pair of strings.
    """
    return _align_genomic_to_genomic(
        genomic_ref, genomic_read, align_threshold,
        score_matrix.matrix,
        score_matrix.insflow_array,
        score_matrix.delflow_array,
        score_matrix.maxscore)


@njit(cache=True)
def _align_genomic_to_genomic(
        genomic_ref, genomic_read, align_threshold,
        matchflow, insflow, delflow, maxscore):
    # genomic must be upper-case DNA and not be empty.
    (bases, _) = (genomic_read.bases, genomic_read.qual)
    (score_threshold, score_possible, _) = get_score_threshold(
        len(genomic_ref), maxscore, align_threshold, len(bases), 0)
    (score, column_index, row_index, traceback_matrix) = compute_dp_matrix_dna(
        genomic_to_dna5_index(genomic_ref),
        genomic_to_dna5_index(bases),
        score_threshold, matchflow, insflow, delflow, maxscore, True)
    (aligned_reference, aligned_read) = traceback(
        genomic_ref, bases, traceback_matrix, column_index, row_index)
    substitue_outer_gaps_with_void(aligned_read)

    if (column_index <= 0) or (score < 0):
        score = 0
    return score, score_possible, (aligned_reference, aligned_read)


def align_genomic_pair_to_genomic(
        genomic_ref, genomic_read1, genomic_read2, align_threshold,
        score_matrix1, score_matrix2,
        mismatch_qual_delta_threshold, insert_qual_threshold):
    """Align `genomic_ref` to a pair of read strings `genomic_read1` and `genomic_read2`
    using score matrices `(matchflow, insflow, delflow, maxscore)`.
    Return `(score, column_index, alignment)`, where `alignment` is a pair of strings.
    """
    return _align_genomic_pair_to_genomic(
        genomic_ref, genomic_read1, genomic_read2, align_threshold,
        score_matrix1.matrix,
        score_matrix1.insflow_array,
        score_matrix1.delflow_array,
        score_matrix1.maxscore,
        score_matrix2.matrix,
        score_matrix2.insflow_array,
        score_matrix2.delflow_array,
        score_matrix2.maxscore,
        mismatch_qual_delta_threshold,
        insert_qual_threshold)


@njit(cache=True)
def _align_genomic_pair_to_genomic(
        genomic_ref, genomic_read1, genomic_read2, align_threshold,
        matchflow, insflow, delflow, maxscore,
        matchflow2, insflow2, delflow2, maxscore2,
        mismatch_qual_delta_threshold, insert_qual_threshold):
    # genomic must be upper-case DNA and not be empty.
    (bases1, qual1) = (genomic_read1.bases, genomic_read1.qual)
    (bases2, qual2) = (genomic_read2.bases, genomic_read2.qual)
    m1 = len(bases1)
    m2 = len(bases2)
    n = len(genomic_ref)

    (_, score_possible1, _) = get_score_threshold(n, maxscore, align_threshold, m1, 0)
    (_, score_possible2, _) = get_score_threshold(n, maxscore, align_threshold, m2, 0)
    (best_threshold1, _, _) = get_score_threshold(max(0, n - m2), maxscore, align_threshold, m1, 0)
    (best_threshold2, _, _) = get_score_threshold(max(0, n - m1), maxscore, align_threshold, m2, 0)

    (score1, column_index1, row_index1, traceback_matrix1) = compute_dp_matrix_dna(
        genomic_to_dna5_index(genomic_ref),
        genomic_to_dna5_index(bases1),
        best_threshold1, matchflow, insflow, delflow, maxscore, True)
    (score2, column_index2, row_index2, traceback_matrix2) = compute_dp_matrix_dna(
        genomic_to_dna5_index(revcomp(genomic_ref)),
        genomic_to_dna5_index(bases2),
        best_threshold2, matchflow2, insflow2, delflow2, maxscore2, True)

    j1, ref_positions1 = traceback_positions(traceback_matrix1, column_index1, row_index1)
    j2, ref_positions2 = traceback_positions(traceback_matrix2, column_index2, row_index2)

    column_index, alignment = merge_aligned_pair(
        bases1, qual1, j1, ref_positions1, bases2, qual2, j2, ref_positions2,
        genomic_ref, mismatch_qual_delta_threshold, insert_qual_threshold)
    score = score1 + score2
    # `substitue_outer_gaps_with_void` not necessary here since already done when merging
    if (column_index <= 0) or (score < 0):
        score = 0

    score_possible = score_possible1 + score_possible2
    return score, score_possible, alignment


ref_pos_dtype = np.int_
qual_dtype = np.int_
AlignedBase = namedtuple('AlignedBase', ['base', 'qual', 'pos'])


@njit(cache=True)
def _merge_reads(read1, read2, mismatch_qual_delta_threshold, insert_qual_threshold):
    pos_delta = read2[0].pos - read1[-1].pos
    Q0 = qual_dtype(0)
    if pos_delta > 0:
        fill = []
        for p in range(read1[-1].pos+1, read2[0].pos):
            fill.append(AlignedBase(VOID, Q0, ref_pos_dtype(p)))
        return read1 + fill + read2
    read = []
    while read1[0].pos < read2[0].pos:
        read.append(read1.pop(0))
    mismatch_qual_delta_threshold = max(0, mismatch_qual_delta_threshold)
    while read1 and read2:
        b1 = read1[0]
        b2 = read2[0]
        if b1.pos < b2.pos:
            if b1.qual >= insert_qual_threshold:
                read.append(b1)
            # else: discard
            read1.pop(0)
        elif b1.pos > b2.pos:
            if b2.qual >= insert_qual_threshold:
                read.append(b2)
            # else: discard
            read2.pop(0)
        else:  # b1.pos == b2.pos
            # TODO: recalculate quality in a reasonable way
            if b1.base == b2.base:
                read.append(AlignedBase(b1.base, b1.qual, b1.pos))
            else:
                if (b1.qual - b2.qual) >= mismatch_qual_delta_threshold:
                    read.append(b1)
                elif (b2.qual - b1.qual) >= mismatch_qual_delta_threshold:
                    read.append(b2)
                else:
                    read.append(AlignedBase(N, Q0, b1.pos))
            read1.pop(0)
            read2.pop(0)
    # append remaining bases if any
    read.extend(read1)
    read.extend(read2)
    return read


@njit(cache=True)
def merge_aligned_pair(
        fwd_bases, fwd_qual, fwd_j, fwd_pos, rev_bases, rev_qual, rev_j, rev_pos,
        reference, mismatch_qual_delta_threshold, insert_qual_threshold):
    read = [AlignedBase(N, qual_dtype(0), ref_pos_dtype(0))]  # type hint
    aligned_reference = [N]  # type hint
    aligned_reference.clear()  # aligned_reference = []
    aligned_read = [N]  # type hint
    aligned_read.clear()  # aligned_read = []

    reference_length = len(reference)
    # read1 = list(zip(fwd_bases[fwd_j:], fwd_qual[fwd_j:], fwd_pos[fwd_j:]))
    read1 = []
    for (b, q, p) in zip(fwd_bases[fwd_j:], fwd_qual[fwd_j:], fwd_pos[fwd_j:]):
        read1.append(AlignedBase(b, q, p))
    rev_len = rev_pos.shape[0]
    rev_bases = revcomp(rev_bases[rev_j:rev_len])
    rev_qual = rev_qual[rev_j:rev_len][::-1]
    # rev_pos = (reference_length - rev_pos[rev_j:] - 1)[::-1]
    for i in range(rev_j, rev_len):
        rev_pos[i] = (reference_length - rev_pos[i] - 1)
    rev_pos = rev_pos[rev_j:rev_len][::-1]
    # read2 = list(zip(rev_bases, rev_qual, rev_pos))
    read2 = []
    for (b, q, p) in zip(rev_bases, rev_qual, rev_pos):
        read2.append(AlignedBase(b, q, p))
    if not read2:
        read = read1
    elif not read1:
        read = read2
    else:
        read = _merge_reads(read1, read2, mismatch_qual_delta_threshold, insert_qual_threshold)
    ref_pos = -1
    column_index = -1
    for column_index, c in enumerate(read):
        while ref_pos+1 < c.pos:
            ref_pos += 1
            aligned_reference.append(reference[ref_pos])
            aligned_read.append(GAP)
        if ref_pos+1 == c.pos:
            ref_pos += 1
            aligned_reference.append(reference[ref_pos])
            aligned_read.append(c.base)
        elif ref_pos == c.pos:
            aligned_reference.append(GAP)
            aligned_read.append(c.base)
        else:
            raise RuntimeError("ref positions in read not ascending")
    while ref_pos < reference_length-1:
        ref_pos += 1
        aligned_reference.append(reference[ref_pos])
        aligned_read.append(VOID)
    for i, b in enumerate(aligned_read):
        if b != GAP:
            break
        aligned_read[i] = VOID
    return column_index, (np.array(aligned_reference), np.array(aligned_read))


@njit(cache=True)
def traceback_positions(traceback_matrix, j, i):
    if j <= 0:  # failure
        return traceback_matrix.shape[0], np.empty(0, dtype=ref_pos_dtype)
    ref_positions = np.empty(j, dtype=ref_pos_dtype)
    while True:
        dd = traceback_matrix[j, i]
        if dd == TRACE.STOP:
            break
        if dd == TRACE.DIAG:
            i -= 1
            j -= 1
            ref_positions[j] = i
        elif dd == TRACE.LEFT:
            j -= 1
            ref_positions[j] = i
        elif dd == TRACE.UP:
            i -= 1
        else:
            raise RuntimeError("INVALID value in traceback matrix")
    return j, ref_positions


@generated_jit(nopython=True, cache=True)
def compute_dp_matrix_dna(
        ix_ref_seq, ix_read_seq, best_threshold,
        match_score, insert_score, delete_score, maxscore, endgapfree=False):
    return make_compute_dp_matrix(make_compute_dp_dna)


@generated_jit(nopython=True, cache=True)
def match_reference_to_dna(
        ix_ref_seq, ix_read_seq, threshold,
        match_score, insert_score, delete_score, maxscore,
        pseudolength, endgapfree):
    """Return best matching score as well as corresponding position in `ix_read_seq`.
    Same as function `compute_dp_matrix` except traceback is omitted here.
    """
    @njit
    def get_max_pseudo_length(ref_length, read_length, pseudo_length):
        return read_length, 0

    return make_match_reference_to_read(make_compute_dp_dna, get_max_pseudo_length)


def make_compute_dp_dna(get_traceback_column, get_score, get_score_no_left, store_score):
    @njit
    def get_delete_score(delete_score):
        return delete_score

    @njit
    def get_delete_curr(delete_score, curr_read):
        return delete_score

    @njit
    def get_insert_curr(insert_score, curr_read, next_read):
        return insert_score

    @njit
    def del_score(delete_curr, curr_ref, next_ref):
        return delete_curr

    @njit
    def ins_score(insert_curr, curr_ref):
        return insert_curr

    @njit
    def get_next_ref(ix_ref_seq, i):
        return None

    @njit
    def get_ref_sentinel(match_score):
        return None

    # NOTE: Keep the above functions _dead simple_! Otherwise they won't get inlined,
    #       which causes massive slowdowns due to call overheads!
    return make_compute_dp(
        get_delete_score, get_delete_curr, get_insert_curr,
        del_score, ins_score, get_next_ref, get_ref_sentinel,
        get_traceback_column, get_score, get_score_no_left, store_score)
