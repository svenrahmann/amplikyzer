from collections import namedtuple
from enum import IntEnum

import numpy as np
from numba import njit

from ...alphabets import GAP, VOID


ScoringMatrices = namedtuple('ScoringMatrices', ['standard', 'bisulfiteCT', 'bisulfiteGA'])


@njit(cache=True)
def get_score_threshold(
        reference_length, max_score, align_threshold, max_length, pseudo_length):
    m = min(reference_length, max_length)
    score_pseudo = max_score * pseudo_length
    score_possible = max(1, max_score * m + score_pseudo)
    score_threshold = int((align_threshold * score_possible) - score_pseudo + 0.5)
    return score_threshold, score_possible, score_pseudo


@njit(cache=True)
def substitue_outer_gaps_with_void(aligned_read):
    for i in range(len(aligned_read)):
        if aligned_read[i] != GAP:
            break
        aligned_read[i] = VOID
    for i in range(len(aligned_read)-1, -1, -1):
        if aligned_read[i] != GAP:
            break
        aligned_read[i] = VOID


MINUS_INFINITY = -999999

TRACE = IntEnum('TRACE', zip(['INVALID', 'STOP', 'DIAG', 'UP', 'LEFT'], range(5)))


@njit(cache=True)
def traceback(reference, read, traceback_matrix, j, i):
    ag, af = [], []
    if j <= 0:  # failure
        return np.array(ag), np.array(af)
    for k in range(len(reference)-1, i-1, -1):
        ag.append(reference[k])
        af.append(GAP)
    while True:
        dd = traceback_matrix[j, i]
        if dd == TRACE.STOP:
            assert i == 0, "traceback did not reach beginning of reference"
            break
        if dd == TRACE.DIAG:
            i -= 1
            j -= 1
            g, f = reference[i], read[j]
        elif dd == TRACE.LEFT:
            j -= 1
            g, f = GAP, read[j]
        elif dd == TRACE.UP:
            i -= 1
            g, f = reference[i], GAP
        else:
            raise RuntimeError("INVALID value in traceback matrix")
        ag.append(g)
        af.append(f)
    return np.array(ag[::-1]), np.array(af[::-1])


def make_compute_dp_matrix(make_compute_dp):
    @njit
    def get_traceback_column(traceback_matrix, j):
        return traceback_matrix[j]

    @njit
    def get_score(score_diag, score_left, score_up):
        scr, trb = score_diag, TRACE.DIAG
        if score_left > scr:
            scr, trb = score_left, TRACE.LEFT
        if score_up > scr:
            scr, trb = score_up, TRACE.UP
        return scr, trb

    @njit
    def get_score_no_left(score_diag, score_up):
        scr, trb = score_diag, TRACE.DIAG
        if score_up > scr:
            scr, trb = score_up, TRACE.UP
        return scr, trb

    @njit
    def store_score(score_column, traceback_column, i, score):
        score_column[i], traceback_column[i] = score

    # NOTE: Keep the above functions _dead_simple_! Otherwise they won't get inlined,
    #       which causes massive slowdowns due to call overheads!
    compute_dp = make_compute_dp(get_traceback_column, get_score, get_score_no_left, store_score)

    def compute_dp_matrix(
            ix_ref_seq, ix_read_seq, best_threshold,
            match_score, insert_score, delete_score, maxscore, endgapfree=False):
        m = len(ix_ref_seq)
        n = len(ix_read_seq)
        assert m > 0, "reference sequence is empty"
        # allocate traceback_matrix[j, i]
        traceback_matrix = np.empty((n+1, m+1), dtype=np.int8)
        if n == 0:
            return 0, -1, m, traceback_matrix

        traceback_matrix[:, 0] = TRACE.STOP  # not LEFT because of "glocal" alignment
        traceback_column = get_traceback_column(traceback_matrix, 0)
        traceback_column[1:] = TRACE.UP

        best_score, best_column_index, best_row_index = compute_dp(
            ix_ref_seq, ix_read_seq, best_threshold,
            match_score, insert_score, delete_score, maxscore,
            endgapfree, traceback_matrix)

        best_score = max(0, best_score)
        return best_score, best_column_index, best_row_index, traceback_matrix

    return compute_dp_matrix


def make_match_reference_to_read(make_compute_dp, get_max_pseudo_length):
    @njit
    def get_traceback_column(traceback_matrix, j):
        return None

    @njit
    def get_score(score_diag, score_left, score_up):
        return max(score_diag, score_left, score_up)

    @njit
    def get_score_no_left(score_diag, score_up):
        return max(score_diag, score_up)

    @njit
    def store_score(score_column, traceback_column, i, score):
        score_column[i] = score

    # NOTE: Keep the above functions _dead simple_! Otherwise they won't get inlined,
    #       which causes massive slowdowns due to call overheads!
    compute_dp = make_compute_dp(get_traceback_column, get_score, get_score_no_left, store_score)

    def match_reference_to_read(
            ix_ref_seq, ix_read_seq, threshold,
            match_score, insert_score, delete_score, maxscore,
            pseudolength, endgapfree):
        m = len(ix_ref_seq)
        n = len(ix_read_seq)
        assert m > 0, "reference sequence is empty"
        if n == 0:
            return -1, -1, m, 0

        # compute score threshold according to arguments
        max_length, pseudolength = get_max_pseudo_length(m, n, pseudolength)
        best_threshold, score_possible, score_pseudo = get_score_threshold(
            m, maxscore, threshold, max_length, pseudolength)
        traceback_matrix = None  # not needed for matching

        best_score, best_column_index, best_row_index = compute_dp(
            ix_ref_seq, ix_read_seq, best_threshold,
            match_score, insert_score, delete_score, maxscore,
            endgapfree, traceback_matrix)

        if best_column_index > 0:
            best_score += score_pseudo
        return best_score, score_possible, best_column_index, best_row_index

    return match_reference_to_read


def make_compute_dp(
        get_delete_score, get_delete_curr, get_insert_curr,
        del_score, ins_score, get_next_ref, get_ref_sentinel,
        get_traceback_column, get_score, get_score_no_left, store_score):
    @njit
    def compute_dp(
            ix_ref_seq, ix_read_seq, best_threshold,
            match_score, insert_score, delete_score, maxscore,
            endgapfree, traceback_matrix):
        m = len(ix_ref_seq)
        n = len(ix_read_seq)
        read_sentinel = match_score.shape[0]
        ref_sentinel = get_ref_sentinel(match_score)
        delete_score_ = get_delete_score(delete_score)

        # allocate column of score matrix
        score_column = np.empty(m+1, dtype=match_score.dtype)
        # column j = 0
        insert_curr = get_insert_curr(insert_score, read_sentinel, ix_read_seq[0])
        lastgoodi = m
        score_column[0] = 0
        for i in range(1, m+1):
            curr_ref = ix_ref_seq[i-1]
            score_column[i] = score_column[i-1] + ins_score(insert_curr, curr_ref)
            if score_column[i] >= best_threshold - (m - i) * maxscore:
                lastgoodi = i

        best_column_index = -1
        best_row_index = m
        best_score = max(best_threshold, score_column[m])  # score_column[m]
        # columns j = 1 .. end
        for j in range(1, n+1):  # iterate over ix_read_seq
            curr_read = ix_read_seq[j-1]
            next_read = ix_read_seq[j] if j < n else read_sentinel
            traceback_column = get_traceback_column(traceback_matrix, j)
            match_curr = match_score[curr_read]
            delete_curr = get_delete_curr(delete_score_, curr_read)
            insert_curr = get_insert_curr(insert_score, curr_read, next_read)
            old_score_column_diag = 0
            for i in range(1, min(lastgoodi+1, m)):
                curr_ref = ix_ref_seq[i-1]
                next_ref = get_next_ref(ix_ref_seq, i)
                score_diag = old_score_column_diag + match_curr[curr_ref]
                score_left = score_column[i] + del_score(delete_curr, curr_ref, next_ref)
                score_up = score_column[i-1] + ins_score(insert_curr, curr_ref)
                old_score_column_diag = score_column[i]
                score = get_score(score_diag, score_left, score_up)
                store_score(score_column, traceback_column, i, score)
            # treat lastgoodi+1 specially: don't look left
            if lastgoodi+1 <= m:
                i = lastgoodi+1
                curr_ref = ix_ref_seq[i-1]
                score_diag = old_score_column_diag + match_curr[curr_ref]
                score_up = score_column[i-1] + ins_score(insert_curr, curr_ref)
                old_score_column_diag = score_column[i]
                score = get_score_no_left(score_diag, score_up)
                store_score(score_column, traceback_column, i, score)
            else:  # we had to compute this all the way down without special treatment
                i += 1
                assert i == lastgoodi == m, "not (i == lastgoodi == m)"
                curr_ref = ix_ref_seq[i-1]
                next_ref = ref_sentinel
                score_diag = old_score_column_diag + match_curr[curr_ref]
                score_left = score_column[i] + del_score(delete_curr, curr_ref, next_ref)
                score_up = score_column[i-1] + ins_score(insert_curr, curr_ref)
                old_score_column_diag = score_column[i]
                score = get_score(score_diag, score_left, score_up)
                store_score(score_column, traceback_column, i, score)
            # compute new lastgoodi
            while score_column[i] < best_threshold - (m - i) * maxscore:
                i -= 1
            lastgoodi = i
            # is j the new best column?
            if lastgoodi == m and score_column[m] > best_score:
                best_column_index = j
                best_score = score_column[m]
                best_threshold = max(best_threshold, best_score)
        if endgapfree:
            for k in range(lastgoodi, -1, -1):
                if score_column[k] > best_score:
                    best_column_index = n
                    best_row_index = k
                    best_score = score_column[k]
        if best_column_index <= 0:  # failure
            best_score = -1
            best_column_index = -1
        return best_score, best_column_index, best_row_index

    return compute_dp
