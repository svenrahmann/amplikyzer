import sys
import gzip
from enum import Enum
from itertools import chain

from .exceptions import AkzrFormatError


class AkzrFile:
    """Reader for a .akzr file.
    The generator `AKZRFile(filename).data()` yields each read's elements.

    The elements are defined by the @ELEMENT and @ALIGNMENT header lines
    within the file.
    """

    READ_FORMAT = Enum("READ_FORMAT", ("FLOWDNA", "GENOMIC"))

    def __init__(self, filename):
        self.filename = filename

    def data(self, info=False):
        if self.filename == "-":
            yield from self._read_data(sys.stdin, info)
        else:
            filename = self.filename
            open_ = gzip.open if filename.endswith(".gz") else open
            with open_(filename, mode="rt") as file:
                yield from self._read_data(file, info)

    def _read_data(self, file, info):
        lines = _getlines(file)
        read_format, elements, next_line = self._read_header(lines)
        if next_line:
            lines = chain([next_line], lines)
        yield read_format, elements
        yield from self._read_entries(lines, elements, info)

    def _read_header(self, lines):
        """Read lines starting with @; they contain the element information."""
        elements = []
        read_format = ""
        for next_line in lines:
            line_number, line = next_line
            if line.startswith(">"):
                break
            if line.startswith("@SFF "):
                if read_format and read_format != self.READ_FORMAT.FLOWDNA:
                    raise AkzrFormatError("inconsistent read format. Line: {}".format(line_number))
                read_format = self.READ_FORMAT.FLOWDNA
                continue
            if line.startswith("@FASTQ "):
                if read_format and read_format != self.READ_FORMAT.GENOMIC:
                    raise AkzrFormatError("inconsistent read format. Line: {}".format(line_number))
                read_format = self.READ_FORMAT.GENOMIC
                continue
            if line.startswith(("@CONF ", "@ARG ")):
                continue
            if not line.startswith(("@ELEMENT ", "@ALIGNMENT ")):
                raise AkzrFormatError(
                    "expected only @ELEMENT or @ALIGNMENT lines."
                    " Line: {}: {}".format(line_number, line))
            elements.append(line.split()[1])
        else:
            next_line = ()
        if not read_format:
            read_format = self.READ_FORMAT.FLOWDNA
        return read_format, elements, next_line

    def _read_entries(self, lines, elements, info):
        if info:
            split = str.split
        else:
            def split(s):
                return s.split()[0]
        for line_number, line in lines:
            # read the information of a single read
            # invariant: current line starts with '>'
            if not line.startswith(">"):
                raise AkzrFormatError(
                    "header line with '>' expected. Line: {}".format(line_number))
            current = dict()
            current['__name__'] = split(line[1:])
            for el, (line_number, line) in zip(elements, lines):
                current[el] = split(line)
            yield current

# end class AKZRFile


def _getlines(f):
    """Get lines from filelike `f`, skipping empty lines and lines with "#"."""
    for line_number, line in enumerate(f):
        line = line.strip()
        if line and line[0] != "#":
            yield line_number, line
