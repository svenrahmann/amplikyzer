# amplikyzer2.main
# (c) Sven Rahmann 2011--2012
"""
Parse the command line arguments and execute the appropriate submodule.
"""

import argparse
from collections import namedtuple
from random import seed
import logging
from time import time

from .analysis import analyze_sff, analyze_fastq
from . import statistics
from . import align
from . import methylation
from . import print_sff
from .constants import EXT_CONFIG

from ._version import get_versions
__version__ = get_versions()["version"]
del get_versions


###################################################################################################
# main; interface for geniegui

def get_argument_parser():
    """Return an ArgumentParser object p with this module's options;
    with an additional dict attribute p._geniegui to specify
    "special" treatment (file/path dialogs) for some options.
    """
    # obtain the ArgumentParser object 'p'
    p = argparse.ArgumentParser(
        prog="amplikyzer2",
        # formatter_class=argparse.ArgumentDefaultsHelpFormatter,
        description="amplikyzer2: an amplicon analyzer",
        epilog=("amplikyzer2 is a research only software by the "
                "Genome Informatics group, University of Duisburg-Essen.")
        )
    p._geniegui = dict()
    # global options for all subcommands
    p.add_argument(
        "--path", "-p", default="",
        help="project path (directory) containing read files")
    p._geniegui["--path"] = "dir"
    p.add_argument(
        "--conf", nargs="+", default=["*"+EXT_CONFIG], metavar="FILE",
        help="names of configuration files with MIDS, TAGS, LOCI, LABELS")
    p.add_argument(
        '-', dest='__narg_delimiter', action="store_true",
        help=argparse.SUPPRESS)
    p.add_argument(
        "--rngseed", type=int, metavar="INT",
        help=argparse.SUPPRESS)
    p.add_argument(
        "--version", action="version",
        version="%(prog)s {}".format(__version__))
    # add subcommands to parser
    SubCommand = namedtuple("SubCommand", ("name", "module", "help_string"))
    subcommands = [  # each module must have a buildparser function and a main function.
        SubCommand("analyzefastq", analyze_fastq,
                   "analyze .fastq files (identify MID, tag, primer, ROI for each read)"),
        SubCommand("statistics", statistics,
                   "show statistics for an analyzed dataset"),
        SubCommand("methylation", methylation,
                   "do a methylation analysis of a given locus and MID"),
        SubCommand("align", align,
                   "output a multiple alignment of all reads of a locus for a given MID"),
        SubCommand("analyzesff", analyze_sff,
                   "analyze .sff files (identify key, MID, tag, primer, ROI for each read)"),
        SubCommand("printsff", print_sff,
                   "print reads of an .sff file"),
    ]
    subs = p.add_subparsers()
    subs.required = True
    subs.dest = "subcommand"
    for subcommand in subcommands:
        subcommand_parser = subs.add_parser(
            # formatter_class=argparse.ArgumentDefaultsHelpFormatter,
            subcommand.name, help=subcommand.help_string, description=subcommand.module.__doc__)
        subcommand_parser._geniegui = dict()
        subcommand_parser.set_defaults(func=subcommand.module.main)
        subcommand.module.buildparser(subcommand_parser)
    return p


class InfoTimeFormatter(logging.Formatter):
    def __init__(self, fmt=None, datefmt=None):
        super().__init__(fmt, datefmt, style="%")
        self.created = time()

    def format(self, record):
        msg = super().format(record)
        if record.levelno != logging.INFO:
            return "{levelname}: {msg}".format(levelname=record.levelname, msg=msg)
        return "@{time:.2f} {msg}".format(time=record.created - self.created, msg=msg)


def main(args=None):
    """main function; interface for geniegui"""
    log_handler = logging.StreamHandler()
    log_handler.setFormatter(InfoTimeFormatter())
    logging.basicConfig(level=logging.INFO, handlers=[log_handler])

    p = get_argument_parser()
    pargs = p.parse_args() if args is None else p.parse_args(args)
    if pargs.rngseed is not None:
        seed(pargs.rngseed)
    pargs.func(pargs)  # call the appropriate subcommand function


__NOTES = """
Description of CWF format and other formats:
http://454.com/my454/documentation/gs-flx-system/emanuals.asp
"""
