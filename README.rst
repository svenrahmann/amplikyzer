*amplikyzer* is short for amplicon analyzer.
It is a software to analyze sequenced amplicons with emphasis on
flowgram analysis (resulting from 454 or IonTorrent sequencing) and methylation analysis after sodium bisulfite treatment.
*amplikyzer2* is the successor to *amplikyzer* and adds support for the analysis of FASTQ reads from Illumina's MiSeq sequencer.

Please read the information in the [Wiki](https://bitbucket.org/svenrahmann/amplikyzer/wiki/Home).
