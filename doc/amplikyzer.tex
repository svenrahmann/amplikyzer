\documentclass[%
fontsize=11pt,
paper=a4,
pagesize, 
BCOR1cm, DIV13,
titlepage, 
parskip=half-, 
numbers=noenddot,
bibliography=totoc,
captions=tablesignature,
]{scrartcl}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage[english]{babel}
\usepackage{url}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{
\begin{tabular}{c}
amplikyzer -- an amplicon analyzer\\
\end{tabular}}

\author{%
Sven Rahmann\\[2ex]
Genome Informatics, Medicine, University of Duisburg-Essen\\
Bioinformatics, Computer Science XI, TU Dortmund
}


\date{\textsc{Work in Progress, \today}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{document}
\selectlanguage{english}
\pagestyle{empty}
\maketitle

\pagestyle{headings}
\pagenumbering{roman}
\setcounter{tocdepth}{2}
\tableofcontents
\pagenumbering{arabic}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

amplikyzer is a software for amplicon sequencing projects, especially in the context of methlyation studies by bisulfite sequencing using technologies that generate sff files (454, IonTorrent).

Features include:
\begin{itemize}
\item alignments are generated directly from flow values, using the original sff file, without intermediate translation to DNA / fasta format; thereby avoiding information loss on the way;
\item tolerance against the homopolymer problem;
\item many customization options for alignments;
\item abilty to output individual and comparative methlyation plots as svg graphics
\end{itemize}

The software, as well as this document, is still considered experimental.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Installation}

\subsection{Windows}

You need Python 3.2 (or higher).
Please refer to \url{http://www.python.org} to download and install Python.
Please remember the Python installation directory (e.g., \texttt{C:\textbackslash Python32}) for later and make sure that in this directory, there exist files \texttt{python.exe} and \texttt{pythonw.exe}.

Download or otherwise obtain the \texttt{amplikyzer.zip} file with several installers (\texttt{geniegui.exe}, \texttt{amplikyzer.exe}, \texttt{sff3.exe}, \texttt{mamaslemonpy.exe}), all of which are required Python packages, and this documentation (\texttt{amplikyzer.pdf}).
Run each installer.

On the desktop, create a new short-cut named ``amplikyzer'' and set it to execute
\begin{center}\ttfamily
C:\textbackslash Python32\textbackslash pythonw.exe -m geniegui amplikyzer
\end{center}
where you replace the path above with the correct location of \texttt{pythonw.exe} on your system.

Click on the shortcut, now a GUI window should open.
Congratulations!

To generate several diagnostic plots using the \texttt{--gnuplot} option, where supported, you need \texttt{gnuplot} from \url{http://sourceforge.net/projects/gnuplot/files/gnuplot}.
Simply run the windows installer and make sure to activate the option that places the gnuplot executable in your path.


\subsection{Linux}
todo

\subsection{MacOS X}
todo


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Basic Usage}

\subsection{Preparations}

Each sequencing project generates its own \texttt{.sff} file.
Each \texttt{.sff} file should be placed in its own separate directory;
amplikyzer will populate such a directory with other files during the analysis.
We refer to such a directory as a \emph{project} directory.

The \texttt{amplikyzer} software will decompose each sequencing read in the \texttt{.sff} file into several parts, according to the typical structure used in amplicon sequencing projects:
\begin{description}
\item[key sequence]: If a read does not start with the key sequence (for 454, it is \texttt{XXXX}), then the read is most certainly invalid and will be discarded
\item[MID] (multiplex ID): todo
\item[tag]: todo
\item[primer]: todo
\item[locus]: todo
\item[remainder]: todo
\end{description}

In order to give amplikyzer the necessary information about the MIDs, tags, primers and loci, so-called \emph{configuration files} (ending in \texttt{.txt}) are required in the project directory.
These can have any name and be any number, but it makes sense to call them precisely
\begin{description}
\item[\texttt{mids.txt}]: contains all MIDs
\item[\texttt{tags.txt}]: contains all (forward and reverse) tags
\item[\texttt{loci.txt}]: contains all primer,locus,primer triples
\end{description}
Hovewer, note that \emph{any} text file ending in \texttt{.txt} in the project directory will be read and parsed.
Make sure that no ``ordinary'' text files (e.g., \texttt{README.txt}) are present, as this will result in errors.
The format of the configuration files is that of standard windows \texttt{.ini} files, so they can be read and parsed by Python's \texttt{ConfigParser}.
Refer to some provided examples for the exact format.

If the reads contain sequence parts (MIDs, tags, primers, loci) that are not present in the configuration file, \emph{they will not be recognized}!
You can use this fact to speed up the analysis process if you are only interested in one particular gene: Put only your gene of interest in \texttt{loci.txt}; this will speed up the analysis considerably.
However, you must re-do the analysis if you later want to include more genes.

Once everything is in place, the idea now is to analyze the reads once (compare the \texttt{.sff} file against all sequences in the configuration files), which takes time(!), and then be able to display different alignments quickly many times without re-analyzing the \texttt{.sff} file.


\subsection{Subcommand Overview}

In order to run \emph{any} amplikyzer step, you \emph{always} need to specify the project directory as the \texttt{--path} option (the topmost option in the GUI).

To analyze any sequencing run, the different subcommands of \texttt{amplikyzer} are typically executed in the following order, assuming all configuration files (\texttt{*.txt}) have been placed into the project directory:
\begin{description}
\item[\texttt{reads}] to view all reads in the sff file in standard fasta format (\texttt{--format dna}) or an extended format (\texttt{--format flowdna}); this can usually be skipped but can be useful for quality control or other purposes. It is \emph{not} required for the analysis!
\item[\texttt{analyze}] to process all reads in the \texttt{.sff} file, compare them against each MID, tag, primer, locus and record the (raw) alignments in the corresponding \texttt{.ampl} file.
This process takes time, but needs to be done only once.
If you try to start it again, it will notice that an \texttt{.ampl} file is already present and refuse to run. Use the \texttt{--force} option to re-start the analysis (e.g., when it did not properly complete previously, or you changed the configuration files).
\item[\texttt{statistics}] to show some interesting statistics (number of reads belonging to each MID, locus, etc.) of the \texttt{.ampl} file (which must have been previously generated by the \texttt{analyze} subcommand, of course).
\item[\texttt{align}] to generate, display and visualize an alignment of reads for a specific MID and locus. 
You can control which alignment columns should be displayed (only C columns of CpGs, all ``interesting'' columns, etc.) using the \texttt{--show} option.
You can generate an image from the CpG alignment (a so-called individual methylation plot).
\item[\texttt{methrates}] to generate, display and visualize methylation rates for a specific locus across all MIDs.
\end{description}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\section{Advanced Topics}

\subsection{Analyzing Several sff Files at Once}

It is possible for the analysis step to have several \texttt{.sff} files in the same directory.
In this case, the analysis step will use them all to generate a single analysis file \verb|__all__.ampl|; these will all use the same configuration files.
This may take some time!

You should verify that no other .ampl files are present in the directory; otherwise subsequent alignment steps will fail.


\subsection{Specifying Alleles for Alignment}

If the genomic sequenc contains wildcards (\texttt{N}s), it is possible to select only those reads to display an alignment which have specific nucleotides at these wildcard locations; this is done using the \texttt{--wildcards} option of the \texttt{align} subcommand.

Minimalistic example: The genomic sequence is \texttt{AAANAAANAAA} with two wildcards, where we assume that the either one can be either \texttt{G} or \texttt{C}.
To select those reads with \texttt{G} at both the first and second location, we specify \texttt{--wildcards GG}.
To select those reads with \texttt{C} at the first location (we don't care about the second one), we specify \texttt{--wildcards CN} or simply \texttt{--wildcards C}, because missing values at the end are always filled with \texttt{N}.
To select those reads with \texttt{C} at the second location (we don't care about the first one), we specify \texttt{--wildcards NC}.


\subsection{Generating Alignments of all MID/locus pairs simultaneously}

Instead of giving specific values for the \texttt{--mid} and \texttt{--locus} options of the \texttt{alignment} subcommand, specify \texttt{*} as value for any of these options to select all MIDs resp.\ loci.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\end{document}