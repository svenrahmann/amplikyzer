Changelog
=========

amplikyzer2 1.2.1 (2017-04-xx)
------------------------------

Changed
~~~~~~~

- Update epilog text


amplikyzer2 1.2.0 (2017-04-03)
------------------------------

Changed
~~~~~~~

- Rename command ``printreads`` to ``printsff``
- Reorder commands, put ``analyzefastq``, ``statistics``, ``metyhlation`` in front
- Default config for ``mid`` element uses ``special_miseq:file:sample_name`` instead of ``special_index``

Fixed
~~~~~

- Fix alignment of single (i.e. non-paired) reads in ``analyzefastq``


amplikyzer2 1.1.6 (2017-03-23)
------------------------------

Fixed
~~~~~

- Revert change from **1.1.5**: properly support ``matplotlib >=2.0`` by using its *classic* style


amplikyzer2 1.1.5 (2017-03-14)
------------------------------

Fixed
~~~~~

- **invalid change, use version 1.1.6**: x-axis label cut off in metyhlation plots when using ``matplotlib >=2.0``


amplikyzer2 1.1.4 (2017-03-12)
------------------------------

Added
~~~~~

- Allow elements in ``analyzefastq`` to use MiSeq metadata (via ``special_miseq:file:...`` or ``special_miseq:read:...``)


amplikyzer2 1.1.3 (2017-02-09)
------------------------------

Fixed
~~~~~

- Broken ``analyzesff`` command

amplikyzer2 1.1.2 (2017-01-24)
------------------------------

Fixed
~~~~~

- Remove redundant %-signs in comparative plots


amplikyzer2 1.1.1 (2017-01-19)
------------------------------

Fixed
~~~~~

- Possibly fix Numba compilation issues (again)


amplikyzer2 1.1.0 (2016-09-19)
------------------------------

Added
~~~~~

- Option ``--badcoverage`` for analysis commands to filter (paired-end) reads based on amount of sequencing gaps
- Choice ``badcoverage`` for analysis option ``--omit``, corresponding to ``--badcoverage`` option
- Option ``methylation --yticklabels`` to specify placement of y-axis labels in comparative plots
- Option ``--seqgap {x,-,N}`` for ``align`` / ``methylation`` commands, making (paired-end) sequence gap characters in output configurable
- Field ``filter`` in analysis files, with which number of filtered forward / reverse reads can be output in ``statistics`` command
- Option ``analyzefastq --scoring`` to specify custom scoring constants
- Option ``analyzefastq --mergequal`` to finetune paired-end read merging
- Option ``--version``

Changed
~~~~~~~

- Skip primers in ``statistics`` command
- Change default analysis output to compressed ``akzr.gz`` files
- Change default analysis output filename to ``analysis.akzr.gz`` when analyzing multiple read sets
- Include mappings of both (paired-end) reads in analysis files (separated by ``|``)
- Rename analysis option ``--discard`` to ``--omit``
- Log read statistics (i.e. filtered, unmapped, unaligned, aligned) during analysis
- Treat gaps at start / end of alignment as sequencing gaps
- Change default scoring constants for ``analyzefastq`` from ``10 -10 -30 -30`` to ``10 -15 -45 -45``; adjust default tresholds in [ELEMENTS] config
- Remove dependency on ``geniegui``
- ``pip`` / ``setuptools`` only: Remove hard dependency on ``matplotlib``; depend on ``matplotlib`` for extra ``amplikyzer2[plot]``
- Rename option ``analysisfile`` to ``--analysisfiles`` in ``statistics`` command

Fixed
~~~~~

- Check for ``matplotlib`` and compatible formats before running analysis and exit early if not satisfiable
- Fix statistics of tags and tag-related elements for paired-end reads (include both mappings, separated by ``|``)
- Possibly fix Numba compilation issues (again)
- Allow omission of element configuration sections
- Workaround Numba compilation issue in ``methylation`` command
- Fix reading from analysis files which have no entries
- Fix analysis output filename deduction

Removed
~~~~~~~

- GUI is now a separate package ``amplikyzer2gui``


amplikyzer2 1.0.2 (2016-08-29)
------------------------------

Removed
~~~~~~~

- Remove explicit ``var`` element from default configuration for ``analyzefastq``

Fixed
~~~~~

- Fix output to stdout in ``analyzefastq``
- Possibly fix Numba compilation issues on Windows


amplikyzer2 1.0.1 (2016-08-26)
------------------------------

Fixed
~~~~~

- Fix ``setuptools`` entry points


amplikyzer2 1.0.0 (2016-08-26)
------------------------------

**amplikyzer2** is the successor to **amplikyzer**!
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

**amplikyzer2** builds on top of *amplikyzer*'s highly customizable analysis capabilities and further adds support for analyzing amplicons from *FASTQ* sources, e.g., as produced by the Illumina *MiSeq* sequencer.
*amplikyzer2* retains all of *amplikyzer*'s features, making them available in FASTQ (Illumina's *MiSeq*) as well as SFF (Roche's *454*) based workflows.

Thanks to the Numba_ compiler, amplicon analyses with *amplikyzer2* are sped up considerably.
For one, this reduced runtimes of typical SFF analyses to few minutes or even seconds.
Secondly, it enabled us to simplify the flowgram processing such that we can skip the sophisticated read mapping step (as described in the `amplikyzer publication`_ and implemented in the `mamaslemonpy package`_) and use a simpler direct alignment instead.

.. _Numba: http://numba.pydata.org/
.. _amplikyzer publication: https://peerj.com/preprints/122/
.. _mamaslemonpy package: https://bitbucket.org/svenrahmann/mamaslemonpy/

A complete list of new features and changes is given below.

Added
~~~~~

- MiSeq support via ``analyzefastq`` command
- Option ``--discard`` for ``analyzefastq`` / ``analyzesff`` with which filtered, unmapped or unaligned reads can be omitted from analysis output files
- Option ``--dpi`` for ``methylation`` command
- Choice ``coverage`` for ``methylation --show`` option
- Extend option ``methylation --format`` to allow output of multiple formats, e.g. ``--format pdf png txt``
- Option ``methylation --snpmeth`` which causes optional methylation sites created by SNPs to be included in methylation analysis
- Ability to read / write to *gzip* compressed analysis files
- Ability (by using ``--mids ?``) to only include MIDs in methylation analysis for which labels are specified (in ``[LABELS]`` config section) [undocumented]
- Print progress (processed reads) in ``align`` / ``methylation`` commands

Changed
~~~~~~~

- Rename ``analyze`` command to ``analyzesff``
- Replace mapping config parameters ``minlen_single``, ``minlen_max``, ``minlen_cum`` with score threshold ``threshold``
- Rename option ``--mode`` to ``--pattern`` for command ``methylation``
- Remove option ``methylation --includemode``; mode / pattern is always part of the output files, now
- Replace option ``--minreads`` of commands ``align`` / ``methylation`` by ``--minreadssff`` and ``--minreadsfastq`` which are conditionally used depending on the reads' origins
- Change default output format of ``methylation --format`` from ``png`` to ``pdf``
- Change arrangement of comparative plots to accommodate larger numbers of MIDs
- Make font size in plots adjust automatically depending on number of methylation sites and / or MIDs
- Allow and ignore space character in locus config entries
- Rename short parameter ``-j`` to ``-p`` in ``align`` and ``methylation`` commands
- Change progress for analysis to display remaining time
- Speed up (still experimental) flow alignment (``analyzesff --alignflows``)
- Depend on ``geniegui >=0.200`` which fixes display of long help texts, etc.

Removed
~~~~~~~

- Remove deprecated options ``--badcpgs``, ``--showpositions``
- Remove ``mamaslemonpy`` dependency, since ``numba`` accelerated alignment is faster
- Remove now defunct ``--indexpath`` option
- Drop Python ``3.3`` support

Fixed
~~~~~

- Fix ``methylation --type comparative`` crashing in ``amplikyzer 0.99.4``
- Fix reading of analysis files from stdin
- Fix writing of analysis files when using more than on SFF input file
- Fix crash in ``methylation`` if there is no methylation site in ROI
- Properly reverse complement IUPAC strings
- Remove inclusion of methylation site from reverse primer in methylation analysis if site lies at start of primer


amplikyzer 0.99.4 (2016-02-11)
------------------------------

Added
~~~~~

- Option ``methylation --combine`` with which multiple analyses can be combined inside one plot (or text output)

Changed
~~~~~~~

- Speed up ``align`` and ``methylation`` commands
- Replace IUPAC-pattern ``HCH`` by ``WCH`` for conversion rate calculation in NOMe-seq analyses


amplikyzer 0.99.3 (2015-11-11)
------------------------------

Fixed
~~~~~

- Revert removal of options in ``0.99.1`` and hide them in GUI instead
- Update dependency to ``geniegui >=0.100``, which supports hidden options


amplikyzer 0.99.2 (2015-11-06)
------------------------------

Changed
~~~~~~~

- Set default dpi for methylation plots to 300


amplikyzer 0.99.1 (2015-11-06)
------------------------------

Changed
~~~~~~~

- Rename methylation analysis modes ``cpg, nome, cpg-nome, gcg-nome, wcg-nome`` to ``cg, gch, hcg, gcg, wcg`` with alias ``nome==gch``

Removed
~~~~~~~

- Remove alias options ``--showpositions`` and ``--badcpgs``
- Remove internal option ``--rngseed``


amplikyzer 0.99 (2015-11-06)
----------------------------

Added
~~~~~

- NOMe-seq methylation analysis support (via ``methylation --mode nome``)
- Parallel execution option (``--parallel`` / ``-j``) for ``align`` and ``methylation`` commands
- Option ``--includemode`` for ``methylation`` command allowing to include analysis mode in filenames
- Option ``--show`` (taking one or more of ``index | position | c-index``) for ``methylation`` command; supercedes ``--showpositions``

Changed
~~~~~~~

- Speed up computations via using ``numba`` and ``numpy``
- Use all available cores for workers in ``analyze --parallel``
- Store sequence primers in analysis files; reuse them in methylation analysis to detect methylation sites at start / end of ROIs
- Preserve order of input reads in analysis files for ``analyze --parallel``
- Replace option ``--badcpgs`` by ``--badmeth``

Deprecated
~~~~~~~~~~

- Make ``--badcpgs`` an alias for ``--badmeth`` and mark it as deprecated
- Make ``--showpositions`` an alias for ``--show position`` and mark it as deprecated


amplikyzer 0.97 (2014-04-23)
----------------------------

Fixed
~~~~~

- Properly close ``matplotlib`` figures in plots


amplikyzer 0.96 (2014-04-22)
----------------------------

Changed
~~~~~~~

- Move ``mamaslemonpy`` and ``geniegui`` to external dependencies
- Move package ``sff3`` to module ``amplikyzer.sff``


amplikyzer 0.95 (2014-04-22)
----------------------------

Initial release
~~~~~~~~~~~~~~~
