from setuptools import setup, find_packages
import versioneer


NAME = "amplikyzer2"
VERSION = versioneer.get_version()
README = open("README.rst").read()
AUTHOR = "Sven Rahmann"
EMAIL = "Sven.Rahmann@gmail.com"
URL = "https://bitbucket.org/svenrahmann/amplikyzer/"
LICENSE = "MIT"
DESCRIPTION = "amplicon methylation analyzer for FASTQ (MiSeq) and SFF (flowgram) files"

CLASSIFIERS = [
    "Development Status :: 5 - Production/Stable",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    "Natural Language :: English",
    "License :: OSI Approved :: MIT License",
    "Operating System :: OS Independent",
    "Programming Language :: Python",
    "Programming Language :: Python :: 3",
    "Programming Language :: Python :: 3.4",
    "Programming Language :: Python :: 3.5",
    "Programming Language :: Python :: 3.6",
    "Topic :: Scientific/Engineering :: Bio-Informatics",
]

python_requires = ">=3.4"  # >=3.4 for `enum`, `re.fullmatch`, `abc.ABC`, `yield from` (3.3)

install_requires = [
    "numpy >=1.9",  # >=0.1.9 for `linspace(.., dtype=...)`, ???
    "numba >=0.26",  # >=0.26 for Enum support
]

extras_require = {
    "plot": ["matplotlib >=1.4"]  # >=1.4 for `matplotlib.style`
}

entry_points = {
    "console_scripts": ["amplikyzer2 = amplikyzer2:main"]
}

setup(
    name=NAME,
    entry_points=entry_points,
    python_requires=python_requires,
    install_requires=install_requires,
    extras_require=extras_require,
    packages=find_packages(),
    version=VERSION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    description=DESCRIPTION,
    long_description=README,
    license=LICENSE,
    classifiers=CLASSIFIERS,
    zip_safe=False,
    cmdclass=versioneer.get_cmdclass(),
)
