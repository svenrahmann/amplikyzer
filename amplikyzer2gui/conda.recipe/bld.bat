@REM NOTE: additional parameters prevent setuptools from downloading during conda build
"%PYTHON%" setup.py install --single-version-externally-managed --record=record.txt
if errorlevel 1 exit 1

if not exist "%PREFIX%\Menu" mkdir "%PREFIX%\Menu"
if errorlevel 1 exit 1
copy "%RECIPE_DIR%\menu-windows.json" "%PREFIX%\Menu\amplikyzer2gui.json"
if errorlevel 1 exit 1

exit 0
