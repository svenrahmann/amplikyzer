#!/bin/bash

# NOTE: additional parameters prevent setuptools from downloading during conda build
$PYTHON setup.py install --single-version-externally-managed --record=record.txt
