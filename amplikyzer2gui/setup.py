from setuptools import setup


NAME = 'amplikyzer2gui'
VERSION = '1.0.1'
AUTHOR = 'Sven Rahmann'
EMAIL = 'Sven.Rahmann@gmail.com'
URL = 'https://bitbucket.org/svenrahmann/amplikyzer/'
LICENSE = 'MIT'
DESCRIPTION = 'GUI for amplikyzer2, an amplicon methylation analyzer'

CLASSIFIERS = [
    'Development Status :: 5 - Production/Stable',
    'Intended Audience :: Developers',
    'Intended Audience :: Science/Research',
    'Natural Language :: English',
    'License :: OSI Approved :: MIT License',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3',
    'Programming Language :: Python :: 3.4',
    'Programming Language :: Python :: 3.5',
    'Programming Language :: Python :: 3.6',
    'Topic :: Scientific/Engineering :: Bio-Informatics',
]

python_requires = '>=3'

install_requires = [
    'geniegui >=0.200',
    'amplikyzer2',
]

py_modules = ['amplikyzer2gui']

entry_points = {
    'gui_scripts': ['amplikyzer2gui = amplikyzer2gui:main']
}

setup(
    name=NAME,
    version=VERSION,
    author=AUTHOR,
    author_email=EMAIL,
    url=URL,
    license=LICENSE,
    description=DESCRIPTION,
    classifiers=CLASSIFIERS,
    python_requires=python_requires,
    install_requires=install_requires,
    py_modules=py_modules,
    entry_points=entry_points,
)
